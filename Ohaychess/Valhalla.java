 package ohaychess;
/**
 *@(#)Valhalla.java 8/21/2006
 *@version 1.3 8/29/2006
 *@author Jesse A. Johnson
 */
import java.util.Stack;
import java.util.Map;
import java.util.HashMap;
/**
 * <pre>Parser - A helper class to HistoryManager.java. Records
 * the location of dead players and resurects that player when called.</pre>
 * 
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
public class Valhalla {
	/**
	 * Constructor to instantiate new graveyard.
	 * A new Valhalla object.
	 */
	public Valhalla() {
		grave = new HashMap<Integer, Stack<Integer>>();
	}
	
	/**
	 * Method to clear all objects from graveyard.
	 */
	public void emptyGrave() {
		grave.clear();
	}
	
	/**
	 * Method to create anamious Stack.
	 * @return Stack<Integer> The new List.
	 */
	private Stack<Integer> newRoster() {
		return new Stack<Integer>();
	}
	
	/**
	 * Method to reanimate dead player.
	 * @param key The lost piece location.
	 * @return int warrior, The lost piece.
	 */
	public int resurrect(int key){
		Stack<Integer> temp = grave.get(key);
		if(temp.empty()){
			System.out.println("Resurrect -1");
			return -1;
		}
		return temp.pop();
	}
	
	/**
	 * Method to send dead player to sleep.
	 * A new Valhalla object.
	 * @param key The lost piece location.
	 * @param warrior The lost piece.
	 */
	public void toOdin(int key, int warrior) {
		if(!grave.containsKey(key)) {
			grave.put(key, newRoster());

		}	
		Stack<Integer> temp = grave.get(key);
		temp.push(warrior);
	}
	
//	---------------------------------------VARIABLES
	private Map<Integer, Stack<Integer>> grave;
}
