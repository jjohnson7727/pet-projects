package ohaychess;
/**
 *@(#)Parser.java 8/18/2006
 *@version 1.2 8/18/2006
 *@author Jesse A. Johnson
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * <pre>Parser - A helper class to parses in game details. First checking 
 * for a valid file using <i>;Title: Yahoo! Chess Game</i> as the checking key.
 * Secondly, it populates an History instance with the game details granted there
 * are no errors.</pre>
 * 
 * <pre>Help Section 1
 * The error <i>The file doesn't appear to be a valid .ohy file</i>,
 * refers to a missing or shifted string <i>;Title: Yahoo! Chess Game</i>.
 * The title string should appear in the first line with <i>;</i> as the first
 * on that line.</pre>
 * 
 * <pre>Help Section 2
 * The error <i>The file contains errors</i>, refers to an incorrect .ohy format.
 * The format should be as follows:
 * 		1. e2-e4 e7-e5 or 12. g1-e2 d4-f3++
 * An incorrect format might be:
 * 		1. e2-e4 e 7-e5 or 2. b1 g8-f6
 * </pre>
 *
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
public class Parser {
	/**
	 * Method to interpret line meaning and assign
	 * to appropriate variable in a History object.
	 * @param s The value to be examined.
	 */
	private static void checkLine(String s) throws IOException {
		if(s.regionMatches(0,";White:",0,7)){
			white = s.substring(8);
		}
		else if(s.regionMatches(0,";Black:",0,7)){
			black = s.substring(8);
		}
		else if(s.regionMatches(0,";Date:",0,6)){
			date = s.substring(7);
		}
		else if(s.equals("")) {
			//do nothing
		}
		else{
		     String[] result = s.split("\\s");
		     for(int x=1; x<result.length; x++)
		        if(result[x].length()>=3){
		        	aList.add(result[x].toString());
		        }
		        else {
		        	History.error = cept2;
		        	throw new IOException();
		        }
		}
	}
	
	/**
	 * Method returns the variable stored in black.
	 * @return String
	 */
	
	public static String getBlack() {
		return black;
	}
	
	/**
	 * Method returns the variable stored in date.
	 * @return String
	 */
	public static String getDate() {
		return date;
	}
	
	/**
	 * Method returns the variables stored in list.
	 * @return String
	 */
	public static ArrayList<String> getList() {
		return aList;
	}
	
	/**
	 * Method returns the variable stored in white.
	 * @return String
	 */
	public static String getWhite() {
		return white;
	}
	
	/**
	 * Method to handle file IO.
	 * @param s The file name to open.
	 */
	public static void parseData(String s) throws IOException {
		aList = new ArrayList<String>();
		boolean isValid = false;
		History.error = "";
			in = new BufferedReader(new FileReader(s));
			String line;
			line = in.readLine();
			if(line.equals(title)) {
				isValid = true;
			}
			else {
				History.error = cept;
				throw new IOException();
			}
			
			while(isValid && ((line = in.readLine()) != null) ) {
				checkLine(line);
			}
	}
	
//	---------------------------------------VARIABLES
	private static final String title = ";Title: Yahoo! Chess Game";
	private static final String cept = "The file doesn't appear to be a valid .ohy file.\nRefer to help section 1.\n";
	private static final String cept2 = "The file contains errors.\nRefer to help section 2.\n";
	
	private static String white, black, date;
	
	private static ArrayList<String> aList;
	private static BufferedReader in;
}
