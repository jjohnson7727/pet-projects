package ohaychess;
/**
 *@(#)History.java 8/18/2006
 *@version 1.2 8/22/2006
 *@author Jesse A. Johnson
 */
import java.io.IOException;
import java.util.ArrayList;
/**
 * <pre>History - A object class for holding and returning game details.</pre>
 *
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
public class History {
	/**
	 * Constructor method to provide default values to variables.
	 */
	public History() {
		white = "";
		black = ""; 
		date = "";
		moveList = new ArrayList<String>();
		position = 0;
	}
	
	/**
	 * Constructor method to provide default values to variables
	 * and open file with given file name.
	 * @param s The file name.
	 */
	public History(String s) {
		white = "";
		black = ""; 
		date = "";
		moveList = new ArrayList<String>();
		position = 0;
		try {
			Parser.parseData(s);
		}
		catch(IOException e) {}
		setVarable();
		lastIndex = moveList.size();
	}
	
//---------------------------------------GETTERS
	/**
	 * Method returns the variable stored in white.
	 * @return String
	 */
	public String getWhite() {
		return white;
	}
	/**
	 * Method returns the variable stored in black.
	 * @return String
	 */
	public String getBlack() {
		return black;
	}
	/**
	 * Method returns the variable stored in date.
	 * @return String
	 */
	public String getDate() {
		return date;
	}
	/**
	 * Method returns the object of elements stored in moveList.
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getHistory(){
		return moveList;
	}
	
	public int getSize() {
		return lastIndex;
	}
	
	public String nextMove() {
		if(position < lastIndex) {
			return moveList.get(position++);
		}
		return END;
	}
	
	public String preMove() {
		//System.out.println("preMove() " + position);
		if(position > 0) {
			return moveList.get(--position);
		}
		return END;
	}

	private void setVarable() {
		white = Parser.getWhite();
		black = Parser.getBlack();
		date = Parser.getDate();
		moveList = Parser.getList();
	}
	public void setPosition() {
		position = 0;
	}

//	---------------------------------------VARIABLES
	private String black; 
	private String date;
	private String white;
	private final String END = "END";
	private ArrayList<String> moveList;
	private static volatile int position;
	private volatile int lastIndex;
	public static String error = "";
	
}//END HISTORY CLASS