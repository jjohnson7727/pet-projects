package ohaychess;
/**
 *@(#)ImagePanel.java
 * @author Jesse A. Johnson
 */
import java.awt.*;
import javax.swing.*;
/** 
 * <pre>ImagePanel - A utility for creating custom JPanels.
 * Class created from an example within "Swing Hacks".
 * Published by O'Reilly Authored by Joshua Marinacci & Chris Adamson 2005
 * Hack #1 pages 1-8.</pre>
 * <pre>Class excepts the picture itself in the constructor. 
 * The picture is set as the background and shape of the panel. 
 * Zeros and null are set to obtain the custom look.</pre>
 *
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 *
 * @author Jesse A. Johnson
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel {
	
	private Image img;
		
	/**
	 * Constructor for creating custom JPanel.
	 * @param img The image for the panel.
	 */
	public ImagePanel(Image img) {
		this.img = img;
		Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(img,0,0,null);
	}
}