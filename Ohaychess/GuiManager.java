package ohaychess;
/**
 *@(#)Valhalla.java 8/24/2006
 *@version 1.0 8/29/2006
 *@author Jesse A. Johnson
 */
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
/**
 * <pre>GuiManager - Contains the main. Specifies how controls work.
 * Centers frame in window.</pre>
 * 
 * Code layout will be as follows for the Unus project:
 * 		Constructors
 * 		Main
 * 		public  (in alphabetic order according to like methods)
 *		private  (in alphabetic order according to like methods)
 * 		variables  (in alphabetic order according to like variables)
 * 
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
@SuppressWarnings("serial")
public class GuiManager extends JFrame {

	/**
	 * Constructor to create new GUI framework.
	 * A new GuiManager object.
	 */
	public GuiManager() {
		super("Ohaychess 5.0");
		setFrame();
		setVisible(true);
	}
	
	/**
	 * Main method to instansitate GuiManager object.
	 * @param args not used
	 */
	public static void main(String[] args) {
		GuiManager gui = new GuiManager();
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Implicit method to add buttons to frame.
	 */
	private void addButtons() {
		back = new ImageButton(new ImageIcon("ohaychess/images/back.gif"));
		end = new ImageButton(new ImageIcon("ohaychess/images/end.gif"));
		next = new ImageButton(new ImageIcon("ohaychess/images/next.gif"));
		start = new ImageButton(new ImageIcon("ohaychess/images/start.gif"));

		buttonPanel.add(start);
		buttonPanel.add(back);
		buttonPanel.add(next);
		buttonPanel.add(end);
		
		setFunctions();
	}
	
	/**
	 * Implicit method to create application window.
	 */
	private void setFrame() {
		
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		double pWidth = screenSize.getWidth() - Math.round((screenSize.getWidth()*25)/100);
		double pHeight = screenSize.getHeight() - Math.round((screenSize.getHeight()*25)/100);

		setSize((int)pWidth, (int)pHeight);
		setLocation( (screenSize.width / 2) - (getWidth() / 2), (screenSize.height / 2) - (getHeight() / 2) ); 
		
		jtp = new JTabbedPane();
		bp = new BoardPanel(getWidth(),getHeight());
		buttonPanel = new JPanel();
		jtp.add(bp);
		
		jtp.setTitleAt(0, "GAME");

		addButtons();
		
		setLayout(new BorderLayout());
		add(jtp, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Implicit method to specify look and control of interface.
	 */
	private void setFunctions() {
		back.setPressedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
		back.setRolloverIcon(new ImageIcon("ohaychess/images/backHover.gif"));
		back.setSelectedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
		back.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
		back.setDisabledIcon(new ImageIcon("ohaychess/images/back.gif"));
		back.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/back.gif"));
		
		end.setPressedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
		end.setRolloverIcon(new ImageIcon("ohaychess/images/endHover.gif"));
		end.setSelectedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
		end.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
		end.setDisabledIcon(new ImageIcon("ohaychess/images/end.gif"));
		end.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/end.gif"));
		
		next.setPressedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
		next.setRolloverIcon(new ImageIcon("ohaychess/images/nextHover.gif"));
		next.setSelectedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
		next.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
		next.setDisabledIcon(new ImageIcon("ohaychess/images/next.gif"));
		next.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/next.gif"));
		
		start.setPressedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
		start.setRolloverIcon(new ImageIcon("ohaychess/images/startHover.gif"));
		start.setSelectedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
		start.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
		start.setDisabledIcon(new ImageIcon("ohaychess/images/start.gif"));
		start.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/start.gif"));

		next.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e){
				bp.nextMove();
				bp.revalidate();
			}
			public void mousePressed(MouseEvent e){}
			public void mouseReleased(MouseEvent e){}
			public void mouseEntered(MouseEvent e){
			}
			public void mouseExited(MouseEvent e){
			}
		});
		
		back.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e){
				bp.preMove();
				bp.revalidate();
			}
			public void mousePressed(MouseEvent e){}
			public void mouseReleased(MouseEvent e){}
			public void mouseEntered(MouseEvent e){
			}
			public void mouseExited(MouseEvent e){
			}
		});
		
		start.addMouseListener(new MouseListener() {
			
			public void mouseClicked(MouseEvent e){
				bp.traverseStart();
			}
			public void mousePressed(MouseEvent e){}
			public void mouseReleased(MouseEvent e){}
			public void mouseEntered(MouseEvent e){
			}
			public void mouseExited(MouseEvent e){
			}
		});

		end.addMouseListener(new MouseListener() {
	
		public void mouseClicked(MouseEvent e){
			bp.traverseEnd();
		}
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
		public void mouseEntered(MouseEvent e){
		}
		public void mouseExited(MouseEvent e){
		}
		});
	}// END OF setFunctions()
	
//	---------------------------------------VARIABLES
	private ImageButton back;
	private ImageButton end;
	private ImageButton next;
	private ImageButton start;
	
	private JTabbedPane jtp;
	private BoardPanel bp;
	private JPanel buttonPanel;
}
