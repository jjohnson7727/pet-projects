package com.jjohnson.ohaychess;
/**
 *@(#)BoardPanel.java 8/24/2006
 *@version 1.0 8/29/2006
 *@author Jesse A. Johnson
 */
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
/**
 * <pre>BoardPanel - Creates a chessboard with figures on a JPanel. 
 * instantiates a new HistoryManager and waites for calls to move
 * from GuiManager.</pre>
 * 
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
@SuppressWarnings("serial")
public class BoardPanel extends JPanel {
	
	/**
	 * Constructor to instantiate new graveyard.
	 * A new Valhalla object.
	 * @param boardWidth GuiManager width to guesstimate our width size
	 * @param boardHeight GuiManager height to guesstimate our height size
	 */
	public BoardPanel(int boardWidth, int boardHeight) {
		hm = new HistoryManager();
		Dimension boardSize;
		if(boardHeight<400) {
			int x = (int)(boardHeight - Math.round((boardHeight*25)/100));
			boardSize = new Dimension(x,x);
		}
		else {
			boardSize = new Dimension(392,392);
		}
 
		chessBoard = new JPanel();
		Border line = BorderFactory.createLineBorder(Color.BLACK, 1);
		chessBoard.setBorder(line);
		chessBoard.setBackground(Color.BLACK);
		
		GridLayout gridLayout = new GridLayout(8, 8);
		gridLayout.setHgap(2);
		gridLayout.setVgap(2);
		chessBoard.setLayout( gridLayout );
		chessBoard.setSize(boardSize);
		chessBoard.setLocation( ( boardWidth/ 2) - (chessBoard.getWidth() / 2), (boardHeight / 2) - (chessBoard.getHeight() / 2)-40 ); 

		for (int i = 0; i < 64; i++) {
			ImagePanel square;
			int row = (i / 8) % 2;
			if (row == 0) {
				square = new ImagePanel( i % 2 == 0 ? new ImageIcon("ohaychess/images/set2/white.png").getImage() :
														new ImageIcon("ohaychess/images/set2/black.png").getImage());
				chessBoard.add( square );
			}
			else	{
				square = new ImagePanel( i % 2 == 0 ? new ImageIcon("ohaychess/images/set2/black.png").getImage() :
														new ImageIcon("ohaychess/images/set2/white.png").getImage());
				chessBoard.add( square );
			}
		}

		setBoard();
		setBackground(Color.DARK_GRAY);
		setLayout(null);
		
		add(chessBoard);
	}

	/**
	 * Method to signal HistoryManager to advance one move
	 * and redraw chessboard.
	 */
	public void nextMove() {
		hm.nextMove();
		redraw();
	}
	
	/**
	 * Method to signal HistoryManager to recede one move
	 * and redraw chessboard.
	 */
	public void preMove() {
		hm.preMove();
		redraw();
	}
	
	/**
	 * Method to signal HistoryManager to iterate to last move
	 * and redraw chessboard.
	 */
	public void traverseEnd() {
		hm.traverseEnd();
		redraw();
	}
	
	/**
	 * Method to signal HistoryManager to reset to starting 
	 * position and redraw chessboard.
	 */
	public void traverseStart() {
		setBoard();
		hm.traverseStart();
		redraw();
	}
	
	/**
	 * Method to set figures in their starting positions.
	 * Position obtained from HistoryManager.
	 */
	public void setBoard() {	
		int figure;
		int position = 0;
		JPanel panel;
		JLabel piece;
		for(int rank=0; rank<8; rank++) {
			for(int file=0; file<8; file++) {
				figure = hm.getFigure(rank, file);
				panel = (JPanel)chessBoard.getComponent(position);
				piece = new JLabel( new ImageIcon("ohaychess/images/set2/" + figure +".png") );
				panel.add(piece);
				piece.setSize(49, 49);
				piece.setLocation(0, 0);
				position++;
			}
		}
	}
	
	/**
	 * Implicit method to set figures to their current positions.
	 * Position obtained from HistoryManager.
	 */
	private void redraw() {
		int figure;
		int position = 0;
		JPanel panel;
		JLabel piece;
		for(int rank=0; rank<8; rank++) {
			for(int file=0; file<8; file++) {
				figure = hm.getFigure(rank, file);
				panel = (JPanel)chessBoard.getComponent(position);
				panel.removeAll();
				piece = new JLabel( new ImageIcon("ohaychess/images/set2/" + figure +".png") );
				panel.add(piece);
				piece.setSize(49, 49);
				piece.setLocation(0, 0);
				position++;
			}
		}
	}
	
//	---------------------------------------VARIABLES
	private JPanel chessBoard;
	private HistoryManager hm;
}
