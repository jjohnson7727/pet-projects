package ohaychess;
/**
 *@(#)ImageButton.java
 * @author Marinacci & Adamson
 */
import java.awt.*;
import javax.swing.*;
/** 
 * <pre>ImageButton - A utility for creating custom JPanels.</pre>
 * 
 * <pre>Class An example from "Swing Hacks".
 * Published by O'Reilly Authored by Joshua Marinacci & Chris Adamson 2005
 * Hack #1 pages 1-8.</pre>
 * <pre>Class excepts either a string as a file name or the picture itself. 
 * The picture is set as the icon for the button. 
 * Zeros and null are set to obtain the custom look.</pre>
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 *
 * @author Jesse A. Johnson
 */

@SuppressWarnings("serial")
public class ImageButton extends JButton {
	
    /**
     * Constructor for creating custom JButtons.
     * @param icon The image for the JPanel.
     */
    public ImageButton(ImageIcon icon) {
        setIcon(icon);
        setMargin(new Insets(0,0,0,0));
        setIconTextGap(0);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setBorder(null);
        setText(null);
        setSize(icon.getImage().getWidth(null),
                icon.getImage().getHeight(null));
    }
}