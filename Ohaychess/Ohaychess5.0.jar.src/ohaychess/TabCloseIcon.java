/*    */ package ohaychess;
/*    */ 
/*    */ import java.awt.Component;
/*    */ import java.awt.Graphics;
/*    */ import java.awt.Rectangle;
/*    */ import java.awt.event.MouseAdapter;
/*    */ import java.awt.event.MouseEvent;
/*    */ import javax.swing.Icon;
/*    */ import javax.swing.ImageIcon;
/*    */ import javax.swing.JTabbedPane;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TabCloseIcon
/*    */   implements Icon
/*    */ {
/*    */   private final Icon mIcon;
/* 25 */   private JTabbedPane mTabbedPane = null;
/* 26 */   private transient Rectangle mPosition = null;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public TabCloseIcon(Icon icon)
/*    */   {
/* 33 */     this.mIcon = icon;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public TabCloseIcon()
/*    */   {
/* 42 */     this(new ImageIcon("ohaychess/images/close.gif"));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void paintIcon(Component c, Graphics g, int x, int y)
/*    */   {
/* 51 */     if (this.mTabbedPane == null)
/*    */     {
/* 53 */       this.mTabbedPane = ((JTabbedPane)c);
/* 54 */       this.mTabbedPane.addMouseListener(new MouseAdapter()
/*    */       {
/*    */ 
/*    */         public void mouseReleased(MouseEvent e)
/*    */         {
/* 59 */           if ((!e.isConsumed()) && (TabCloseIcon.this.mPosition.contains(e.getX(), e.getY())))
/*    */           {
/* 61 */             int index = TabCloseIcon.this.mTabbedPane.getSelectedIndex();
/* 62 */             TabCloseIcon.this.mTabbedPane.remove(index);
/* 63 */             e.consume();
/*    */           }
/*    */         }
/*    */       });
/*    */     }
/*    */     
/* 69 */     this.mPosition = new Rectangle(x, y, getIconWidth(), getIconHeight());
/* 70 */     this.mIcon.paintIcon(c, g, x, y);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getIconWidth()
/*    */   {
/* 79 */     return this.mIcon.getIconWidth();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public int getIconHeight()
/*    */   {
/* 87 */     return this.mIcon.getIconHeight();
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/TabCloseIcon.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */