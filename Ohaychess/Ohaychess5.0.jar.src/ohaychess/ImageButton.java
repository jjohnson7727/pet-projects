/*    */ package ohaychess;
/*    */ 
/*    */ import java.awt.Image;
/*    */ import java.awt.Insets;
/*    */ import javax.swing.ImageIcon;
/*    */ import javax.swing.JButton;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ImageButton
/*    */   extends JButton
/*    */ {
/*    */   public ImageButton(ImageIcon icon)
/*    */   {
/* 30 */     setIcon(icon);
/* 31 */     setMargin(new Insets(0, 0, 0, 0));
/* 32 */     setIconTextGap(0);
/* 33 */     setContentAreaFilled(false);
/* 34 */     setBorderPainted(false);
/* 35 */     setBorder(null);
/* 36 */     setText(null);
/* 37 */     setSize(icon.getImage().getWidth(null), 
/* 38 */       icon.getImage().getHeight(null));
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/ImageButton.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */