/*     */ package ohaychess;
/*     */ 
/*     */ import java.awt.GridBagConstraints;
/*     */ import java.awt.Insets;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GBC
/*     */   extends GridBagConstraints
/*     */ {
/*     */   public GBC(int gridx, int gridy)
/*     */   {
/*  39 */     this.gridx = gridx;
/*  40 */     this.gridy = gridy;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC(int gridx, int gridy, int gridwidth, int gridheight)
/*     */   {
/*  53 */     this.gridx = gridx;
/*  54 */     this.gridy = gridy;
/*  55 */     this.gridwidth = gridwidth;
/*  56 */     this.gridheight = gridheight;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setAnchor(int anchor)
/*     */   {
/*  66 */     this.anchor = anchor;
/*  67 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setFill(int fill)
/*     */   {
/*  77 */     this.fill = fill;
/*  78 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setWeight(double weightx, double weighty)
/*     */   {
/*  89 */     this.weightx = weightx;
/*  90 */     this.weighty = weighty;
/*  91 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setInsets(int distance)
/*     */   {
/* 101 */     this.insets = new Insets(distance, distance, distance, distance);
/* 102 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setInsets(int top, int left, int bottom, int right)
/*     */   {
/* 115 */     this.insets = new Insets(top, left, bottom, right);
/* 116 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public GBC setIpad(int ipadx, int ipady)
/*     */   {
/* 127 */     this.ipadx = ipadx;
/* 128 */     this.ipady = ipady;
/* 129 */     return this;
/*     */   }
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/GBC.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */