/*     */ package ohaychess;
/*     */ 
/*     */ import java.awt.BorderLayout;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.Toolkit;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.awt.event.MouseEvent;
/*     */ import java.awt.event.MouseListener;
/*     */ import java.io.File;
/*     */ import javax.swing.ImageIcon;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JMenu;
/*     */ import javax.swing.JMenuBar;
/*     */ import javax.swing.JMenuItem;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JTabbedPane;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GuiManager
/*     */   extends JFrame
/*     */ {
/*     */   private double pWidth;
/*     */   private double pHeight;
/*     */   private ImageButton back;
/*     */   private ImageButton end;
/*     */   private ImageButton next;
/*     */   private ImageButton start;
/*     */   private JTabbedPane jtp;
/*     */   private JPanel buttonPanel;
/*     */   private JMenuBar menuBar;
/*     */   private JMenu file;
/*     */   private JMenuItem open;
/*     */   private JMenuItem flip;
/*     */   private Dimension screenSize;
/*     */   
/*     */   public GuiManager()
/*     */   {
/*  48 */     super("Ohaychess 5.0");
/*  49 */     setFrame();
/*  50 */     setVisible(true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void main(String[] args)
/*     */   {
/*  58 */     GuiManager gui = new GuiManager();
/*  59 */     gui.setDefaultCloseOperation(3);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void addButtons()
/*     */   {
/*  66 */     this.back = new ImageButton(new ImageIcon("ohaychess/images/back.gif"));
/*  67 */     this.end = new ImageButton(new ImageIcon("ohaychess/images/end.gif"));
/*  68 */     this.next = new ImageButton(new ImageIcon("ohaychess/images/next.gif"));
/*  69 */     this.start = new ImageButton(new ImageIcon("ohaychess/images/start.gif"));
/*     */     
/*  71 */     this.buttonPanel.add(this.start);
/*  72 */     this.buttonPanel.add(this.back);
/*  73 */     this.buttonPanel.add(this.next);
/*  74 */     this.buttonPanel.add(this.end);
/*     */     
/*  76 */     setFunctions();
/*     */   }
/*     */   
/*     */   private BoardPanel getBoard() {
/*  80 */     return (BoardPanel)this.jtp.getSelectedComponent();
/*     */   }
/*     */   
/*     */   private void openFile() {
/*  84 */     JFileChooser fc = new JFileChooser();
/*     */     
/*     */ 
/*  87 */     ExtensionFileFilter filter = new ExtensionFileFilter();
/*  88 */     filter.addExtension("txt");
/*  89 */     fc.setFileFilter(filter);
/*     */     
/*  91 */     fc.setCurrentDirectory(new File(".." + File.separator + "Unus"));
/*  92 */     fc.showOpenDialog(this);
/*  93 */     File file = fc.getSelectedFile();
/*  94 */     BoardPanel bp = new BoardPanel(file.getName());
/*  95 */     this.jtp.addTab(file.getName(), new TabCloseIcon(), bp);
/*  96 */     this.jtp.setSelectedComponent(bp);
/*     */     
/*  98 */     setVisible(true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void setFrame()
/*     */   {
/* 106 */     Toolkit kit = Toolkit.getDefaultToolkit();
/* 107 */     this.screenSize = kit.getScreenSize();
/* 108 */     this.pWidth = (this.screenSize.getWidth() - Math.round(this.screenSize.getWidth() * 25.0D / 100.0D));
/* 109 */     this.pHeight = (this.screenSize.getHeight() - Math.round(this.screenSize.getHeight() * 25.0D / 100.0D));
/*     */     
/* 111 */     setSize((int)this.pWidth, (int)this.pHeight + 25);
/* 112 */     setLocation(this.screenSize.width / 2 - getWidth() / 2, this.screenSize.height / 2 - getHeight() / 2);
/*     */     
/* 114 */     this.jtp = new JTabbedPane();
/* 115 */     this.buttonPanel = new JPanel();
/* 116 */     addButtons();
/*     */     
/* 118 */     setLayout(new BorderLayout());
/* 119 */     add(this.jtp, "Center");
/* 120 */     add(this.buttonPanel, "South");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void setFunctions()
/*     */   {
/* 127 */     this.open = new JMenuItem("Open");
/* 128 */     this.open.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent e) {
/* 130 */         GuiManager.this.openFile();
/*     */       }
/*     */       
/* 133 */     });
/* 134 */     this.flip = new JMenuItem("Flip board");
/* 135 */     this.flip.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent e) {
/* 137 */         BoardPanel bp = GuiManager.this.getBoard();
/* 138 */         bp.flip();
/*     */       }
/*     */       
/* 141 */     });
/* 142 */     this.file = new JMenu("File");
/* 143 */     this.file.add(this.open);
/* 144 */     this.file.add(this.flip);
/*     */     
/* 146 */     this.menuBar = new JMenuBar();
/* 147 */     this.menuBar.add(this.file);
/*     */     
/* 149 */     setJMenuBar(this.menuBar);
/*     */     
/* 151 */     this.back.setPressedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
/* 152 */     this.back.setRolloverIcon(new ImageIcon("ohaychess/images/backHover.gif"));
/* 153 */     this.back.setSelectedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
/* 154 */     this.back.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/backDown.gif"));
/* 155 */     this.back.setDisabledIcon(new ImageIcon("ohaychess/images/back.gif"));
/* 156 */     this.back.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/back.gif"));
/*     */     
/* 158 */     this.end.setPressedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
/* 159 */     this.end.setRolloverIcon(new ImageIcon("ohaychess/images/endHover.gif"));
/* 160 */     this.end.setSelectedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
/* 161 */     this.end.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/endDown.gif"));
/* 162 */     this.end.setDisabledIcon(new ImageIcon("ohaychess/images/end.gif"));
/* 163 */     this.end.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/end.gif"));
/*     */     
/* 165 */     this.next.setPressedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
/* 166 */     this.next.setRolloverIcon(new ImageIcon("ohaychess/images/nextHover.gif"));
/* 167 */     this.next.setSelectedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
/* 168 */     this.next.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/nextDown.gif"));
/* 169 */     this.next.setDisabledIcon(new ImageIcon("ohaychess/images/next.gif"));
/* 170 */     this.next.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/next.gif"));
/*     */     
/* 172 */     this.start.setPressedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
/* 173 */     this.start.setRolloverIcon(new ImageIcon("ohaychess/images/startHover.gif"));
/* 174 */     this.start.setSelectedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
/* 175 */     this.start.setRolloverSelectedIcon(new ImageIcon("ohaychess/images/startDown.gif"));
/* 176 */     this.start.setDisabledIcon(new ImageIcon("ohaychess/images/start.gif"));
/* 177 */     this.start.setDisabledSelectedIcon(new ImageIcon("ohaychess/images/start.gif"));
/*     */     
/* 179 */     this.next.addMouseListener(new MouseListener() {
/*     */       public void mouseClicked(MouseEvent e) {
/* 181 */         BoardPanel bp = GuiManager.this.getBoard();
/* 182 */         bp.nextMove();
/* 183 */         bp.revalidate();
/*     */       }
/*     */       
/*     */       public void mousePressed(MouseEvent e) {}
/*     */       
/*     */       public void mouseReleased(MouseEvent e) {}
/*     */       
/*     */       public void mouseEntered(MouseEvent e) {}
/*     */       
/* 192 */       public void mouseExited(MouseEvent e) {} });
/* 193 */     this.back.addMouseListener(new MouseListener() {
/*     */       public void mouseClicked(MouseEvent e) {
/* 195 */         BoardPanel bp = GuiManager.this.getBoard();
/* 196 */         bp.preMove();
/* 197 */         bp.revalidate();
/*     */       }
/*     */       
/*     */       public void mousePressed(MouseEvent e) {}
/*     */       
/*     */       public void mouseReleased(MouseEvent e) {}
/*     */       
/*     */       public void mouseEntered(MouseEvent e) {}
/*     */       
/* 206 */       public void mouseExited(MouseEvent e) {} });
/* 207 */     this.start.addMouseListener(new MouseListener() {
/*     */       public void mouseClicked(MouseEvent e) {
/* 209 */         BoardPanel bp = GuiManager.this.getBoard();
/* 210 */         bp.traverseStart();
/*     */       }
/*     */       
/*     */       public void mousePressed(MouseEvent e) {}
/*     */       
/*     */       public void mouseReleased(MouseEvent e) {}
/*     */       
/*     */       public void mouseEntered(MouseEvent e) {}
/*     */       
/* 219 */       public void mouseExited(MouseEvent e) {} });
/* 220 */     this.end.addMouseListener(new MouseListener() {
/*     */       public void mouseClicked(MouseEvent e) {
/* 222 */         BoardPanel bp = GuiManager.this.getBoard();
/* 223 */         bp.traverseEnd();
/*     */       }
/*     */       
/*     */       public void mousePressed(MouseEvent e) {}
/*     */       
/*     */       public void mouseReleased(MouseEvent e) {}
/*     */       
/*     */       public void mouseEntered(MouseEvent e) {}
/*     */       
/* 232 */       public void mouseExited(MouseEvent e) {} });
/* 233 */     this.jtp.addMouseListener(new MouseListener()
/*     */     {
/*     */       public void mouseClicked(MouseEvent e) {}
/*     */       
/*     */       public void mousePressed(MouseEvent e) {}
/*     */       
/*     */       public void mouseReleased(MouseEvent e) {}
/*     */       
/*     */       public void mouseEntered(MouseEvent e) {}
/*     */       
/*     */       public void mouseExited(MouseEvent e) {}
/*     */     });
/*     */   }
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/GuiManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */