/*    */ package ohaychess;
/*    */ 
/*    */ import javax.swing.ImageIcon;
/*    */ 
/*    */ public class ScaledImageMap
/*    */ {
/*    */   java.util.HashMap<String, ImageIcon> list;
/*    */   
/*    */   public ScaledImageMap()
/*    */   {
/* 11 */     this.list = new java.util.HashMap();
/*    */   }
/*    */   
/*    */   public void addImage(String imageName) {
/* 15 */     java.awt.Toolkit kit = java.awt.Toolkit.getDefaultToolkit();
/* 16 */     java.awt.Dimension screenSize = kit.getScreenSize();
/* 17 */     int imageSize = (int)Math.round(screenSize.getWidth() * 0.05D);
/*    */     
/* 19 */     java.awt.Image image = new ImageIcon(imageName).getImage().getScaledInstance(imageSize, imageSize, 2);
/* 20 */     ImageIcon icon = new ImageIcon(image);
/* 21 */     this.list.put(imageName, icon);
/*    */   }
/*    */   
/*    */   public ImageIcon getImage(String imageName) {
/* 25 */     return (ImageIcon)this.list.get(imageName);
/*    */   }
/*    */   
/*    */   public int getSize(String imageName) {
/* 29 */     ImageIcon icon = (ImageIcon)this.list.get(imageName);
/* 30 */     return icon.getIconWidth();
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/ScaledImageMap.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */