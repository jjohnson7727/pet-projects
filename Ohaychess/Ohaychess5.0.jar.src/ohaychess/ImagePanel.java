/*    */ package ohaychess;
/*    */ 
/*    */ import java.awt.Dimension;
/*    */ import java.awt.Graphics;
/*    */ import java.awt.Image;
/*    */ import java.awt.Toolkit;
/*    */ import javax.swing.JPanel;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ImagePanel
/*    */   extends JPanel
/*    */ {
/*    */   private int imageSize;
/*    */   private Image img;
/*    */   
/*    */   public ImagePanel()
/*    */   {
/* 30 */     this.img = null;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public ImagePanel(Image img)
/*    */   {
/* 38 */     this.img = img;
/*    */     
/* 40 */     Toolkit kit = Toolkit.getDefaultToolkit();
/* 41 */     Dimension screenSize = kit.getScreenSize();
/* 42 */     this.imageSize = ((int)Math.round(screenSize.getWidth() * 0.05D));
/*    */     
/* 44 */     Dimension size = new Dimension(this.imageSize, this.imageSize);
/* 45 */     setPreferredSize(size);
/* 46 */     setMinimumSize(size);
/* 47 */     setMaximumSize(size);
/* 48 */     setSize(size);
/* 49 */     setBackground(null);
/* 50 */     setLayout(null);
/*    */   }
/*    */   
/*    */   public void paintComponent(Graphics g) {
/* 54 */     g.drawImage(this.img, 0, 0, this.imageSize, this.imageSize, null);
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/ImagePanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */