/*    */ package ohaychess;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ import java.util.Stack;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Valhalla
/*    */ {
/*    */   private Map<Integer, Stack<Integer>> grave;
/*    */   
/*    */   public Valhalla()
/*    */   {
/* 22 */     this.grave = new HashMap();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void emptyGrave()
/*    */   {
/* 29 */     this.grave.clear();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private Stack<Integer> newRoster()
/*    */   {
/* 37 */     return new Stack();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public int resurrect(int key)
/*    */   {
/* 46 */     Stack<Integer> temp = (Stack)this.grave.get(Integer.valueOf(key));
/* 47 */     if (temp.empty()) {
/* 48 */       System.out.println("Resurrect -1");
/* 49 */       return -1;
/*    */     }
/* 51 */     return ((Integer)temp.pop()).intValue();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void toOdin(int key, int warrior)
/*    */   {
/* 61 */     if (!this.grave.containsKey(Integer.valueOf(key))) {
/* 62 */       this.grave.put(Integer.valueOf(key), newRoster());
/*    */     }
/*    */     
/* 65 */     Stack<Integer> temp = (Stack)this.grave.get(Integer.valueOf(key));
/* 66 */     temp.push(Integer.valueOf(warrior));
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/Valhalla.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */