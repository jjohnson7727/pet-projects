/*    */ package ohaychess;
/*    */ 
/*    */ import java.io.File;
/*    */ import java.util.ArrayList;
/*    */ import javax.swing.filechooser.FileFilter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ class ExtensionFileFilter
/*    */   extends FileFilter
/*    */ {
/*    */   public void addExtension(String extension)
/*    */   {
/* 18 */     if (!extension.startsWith("."))
/* 19 */       extension = "." + extension;
/* 20 */     this.extensions.add(extension.toLowerCase());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setDescription(String aDescription)
/*    */   {
/* 30 */     this.description = aDescription;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getDescription()
/*    */   {
/* 40 */     return this.description;
/*    */   }
/*    */   
/*    */   public boolean accept(File f)
/*    */   {
/* 45 */     if (f.isDirectory()) return true;
/* 46 */     String name = f.getName().toLowerCase();
/*    */     
/*    */ 
/* 49 */     for (String extension : this.extensions)
/* 50 */       if (name.endsWith(extension))
/* 51 */         return true;
/* 52 */     return false;
/*    */   }
/*    */   
/* 55 */   private String description = "";
/* 56 */   private ArrayList<String> extensions = new ArrayList();
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/ExtensionFileFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */