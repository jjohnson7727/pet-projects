/*    */ package ohaychess;
/*    */ 
/*    */ import java.awt.Color;
/*    */ import java.awt.Graphics;
/*    */ import java.awt.Rectangle;
/*    */ import javax.swing.Icon;
/*    */ 
/*    */ class CloseTabIcon implements Icon
/*    */ {
/*    */   private int x_pos;
/*    */   private int y_pos;
/*    */   private int width;
/*    */   private int height;
/*    */   private Icon fileIcon;
/*    */   
/*    */   public CloseTabIcon(Icon fileIcon)
/*    */   {
/* 18 */     this.fileIcon = fileIcon;
/* 19 */     this.width = 16;
/* 20 */     this.height = 16;
/*    */   }
/*    */   
/*    */   public void paintIcon(java.awt.Component c, Graphics g, int x, int y) {
/* 24 */     this.x_pos = x;
/* 25 */     this.y_pos = y;
/* 26 */     Color col = g.getColor();
/* 27 */     g.setColor(Color.black);
/* 28 */     int y_p = y + 2;
/* 29 */     g.drawLine(x + 1, y_p, x + 12, y_p);
/* 30 */     g.drawLine(x + 1, y_p + 13, x + 12, y_p + 13);
/* 31 */     g.drawLine(x, y_p + 1, x, y_p + 12);
/* 32 */     g.drawLine(x + 13, y_p + 1, x + 13, y_p + 12);
/* 33 */     g.drawLine(x + 3, y_p + 3, x + 10, y_p + 10);
/* 34 */     g.drawLine(x + 3, y_p + 4, x + 9, y_p + 10);
/* 35 */     g.drawLine(x + 4, y_p + 3, x + 10, y_p + 9);
/* 36 */     g.drawLine(x + 10, y_p + 3, x + 3, y_p + 10);
/* 37 */     g.drawLine(x + 10, y_p + 4, x + 4, y_p + 10);
/* 38 */     g.drawLine(x + 9, y_p + 3, x + 3, y_p + 9);
/* 39 */     g.setColor(col);
/* 40 */     if (this.fileIcon != null) {
/* 41 */       this.fileIcon.paintIcon(c, g, x + this.width, y_p);
/*    */     }
/*    */   }
/*    */   
/*    */   public int getIconWidth() {
/* 46 */     return this.width + (this.fileIcon != null ? this.fileIcon.getIconWidth() : 0);
/*    */   }
/*    */   
/*    */   public int getIconHeight() {
/* 50 */     return this.height;
/*    */   }
/*    */   
/*    */   public Rectangle getBounds() {
/* 54 */     return new Rectangle(this.x_pos, this.y_pos, this.width, this.height);
/*    */   }
/*    */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/CloseTabIcon.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */