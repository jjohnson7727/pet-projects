/*     */ package ohaychess;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.FileReader;
/*     */ import java.io.IOException;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Parser
/*     */ {
/*     */   private void checkLine(String s)
/*     */     throws IOException
/*     */   {
/*  40 */     if (s.regionMatches(0, ";White:", 0, 7)) {
/*  41 */       this.white = s.substring(8);
/*     */     }
/*  43 */     else if (s.regionMatches(0, ";Black:", 0, 7)) {
/*  44 */       this.black = s.substring(8);
/*     */     }
/*  46 */     else if (s.regionMatches(0, ";Date:", 0, 6)) {
/*  47 */       this.date = s.substring(7);
/*     */     }
/*  49 */     else if (!s.equals(""))
/*     */     {
/*     */ 
/*     */ 
/*  53 */       String[] result = s.split("\\s");
/*  54 */       for (int x = 1; x < result.length; x++) {
/*  55 */         if (result[x].length() >= 3) {
/*  56 */           this.aList.add(result[x].toString());
/*     */         }
/*     */         else {
/*  59 */           throw new IOException("The file contains errors.\nRefer to help section 2.\n");
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getBlack()
/*     */   {
/*  70 */     return this.black;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getDate()
/*     */   {
/*  78 */     return this.date;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public ArrayList<String> getList()
/*     */   {
/*  86 */     return this.aList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getWhite()
/*     */   {
/*  94 */     return this.white;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void parseData(String s)
/*     */     throws IOException
/*     */   {
/* 102 */     this.aList = new ArrayList();
/* 103 */     boolean isValid = false;
/* 104 */     this.in = new BufferedReader(new FileReader(s));
/*     */     
/* 106 */     String line = this.in.readLine();
/* 107 */     if (line.equals(";Title: Yahoo! Chess Game")) {
/* 108 */       isValid = true;
/*     */     }
/*     */     else {
/* 111 */       throw new IOException("The file doesn't appear to be a valid .ohy file.\nRefer to help section 1.\n");
/*     */     }
/*     */     
/* 114 */     while ((isValid) && ((line = this.in.readLine()) != null)) {
/* 115 */       checkLine(line);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/* 120 */   private final String title = ";Title: Yahoo! Chess Game";
/* 121 */   private final String cept = "The file doesn't appear to be a valid .ohy file.\nRefer to help section 1.\n";
/* 122 */   private final String cept2 = "The file contains errors.\nRefer to help section 2.\n";
/*     */   private String white;
/*     */   private String black;
/*     */   private String date;
/*     */   private ArrayList<String> aList;
/*     */   private BufferedReader in;
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/Parser.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */