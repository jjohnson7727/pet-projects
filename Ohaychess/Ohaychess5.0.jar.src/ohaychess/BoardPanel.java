/*     */ package ohaychess;
/*     */ 
/*     */ import java.awt.Color;
/*     */ import java.awt.GridBagLayout;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.ImageIcon;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.border.Border;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BoardPanel
/*     */   extends JPanel
/*     */ {
/*     */   private boolean flip;
/*     */   private ScaledImageMap scaledImageMap;
/*     */   private JPanel chessboard;
/*     */   private JPanel constricter;
/*     */   private HistoryManager hm;
/*     */   
/*     */   public BoardPanel() {}
/*     */   
/*     */   public BoardPanel(String s)
/*     */   {
/*  34 */     this.hm = new HistoryManager(s);
/*  35 */     this.flip = false;
/*  36 */     setup();
/*  37 */     setBoard();
/*     */   }
/*     */   
/*     */   public void flip() {
/*  41 */     this.flip = (!this.flip);
/*  42 */     this.constricter.removeAll();
/*  43 */     flipUpdateUI();
/*  44 */     redraw();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextMove()
/*     */   {
/*  52 */     this.hm.nextMove();
/*  53 */     redraw();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void preMove()
/*     */   {
/*  61 */     this.hm.preMove();
/*  62 */     redraw();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void traverseEnd()
/*     */   {
/*  70 */     this.hm.traverseEnd();
/*  71 */     redraw();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void traverseStart()
/*     */   {
/*  79 */     this.hm.traverseStart();
/*  80 */     redraw();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setBoard()
/*     */   {
/*  89 */     int position = 0;
/*     */     
/*     */ 
/*     */ 
/*  93 */     for (int rank = 0; rank < 8; rank++) {
/*  94 */       for (int file = 0; file < 8; file++) { int figure;
/*  95 */         int figure; if (this.flip) figure = this.hm.getFlippedFigure(rank, file); else
/*  96 */           figure = this.hm.getFigure(rank, file);
/*  97 */         JPanel panel = (JPanel)this.chessboard.getComponent(position);
/*  98 */         this.scaledImageMap.addImage("ohaychess/images/set2/" + figure + ".png");
/*  99 */         JLabel piece = new JLabel(this.scaledImageMap.getImage("ohaychess/images/set2/" + figure + ".png"));
/* 100 */         panel.add(piece);
/* 101 */         piece.setSize(this.scaledImageMap.getSize("ohaychess/images/set2/" + figure + ".png"), 
/* 102 */           this.scaledImageMap.getSize("ohaychess/images/set2/" + figure + ".png"));
/* 103 */         piece.setLocation(0, 0);
/* 104 */         position++;
/*     */       }
/*     */     }
/* 107 */     this.scaledImageMap.addImage("ohaychess/images/set2/13.png");
/* 108 */     this.scaledImageMap.addImage("ohaychess/images/set2/14.png");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private void setup()
/*     */   {
/* 115 */     setLayout(new GridBagLayout());
/*     */     
/*     */ 
/* 118 */     this.chessboard = new JPanel();
/* 119 */     this.constricter = new JPanel();
/* 120 */     this.constricter.setBackground(null);
/*     */     
/*     */ 
/* 123 */     Border raised = BorderFactory.createEtchedBorder(0);
/* 124 */     Border redline = BorderFactory.createLineBorder(new Color(128, 128, 64), 2);
/* 125 */     this.chessboard.setBorder(BorderFactory.createCompoundBorder(
/* 126 */       redline, raised));
/*     */     
/* 128 */     this.chessboard.setLayout(new GridBagLayout());
/* 129 */     this.constricter.setLayout(new GridBagLayout());
/*     */     
/* 131 */     this.scaledImageMap = new ScaledImageMap();
/*     */     
/* 133 */     setBackground(Color.DARK_GRAY);
/*     */     
/* 135 */     int i = 0;
/*     */     
/* 137 */     for (int rank = 0; rank < 8; rank++) {
/* 138 */       for (int file = 0; file < 8; file++) {
/* 139 */         int row = i / 8 % 2;
/* 140 */         if (row == 0) {
/* 141 */           ImagePanel square = new ImagePanel(i % 2 == 0 ? 
/* 142 */             new ImageIcon("ohaychess/images/set2/white.png").getImage() : 
/* 143 */             new ImageIcon("ohaychess/images/set2/black.png").getImage());
/* 144 */           this.chessboard.add(square, new GBC(file, rank).setWeight(0.0D, 0.0D));
/*     */         }
/*     */         else {
/* 147 */           ImagePanel square = new ImagePanel(i % 2 == 0 ? 
/* 148 */             new ImageIcon("ohaychess/images/set2/black.png").getImage() : 
/* 149 */             new ImageIcon("ohaychess/images/set2/white.png").getImage());
/* 150 */           this.chessboard.add(square, new GBC(file, rank).setWeight(0.0D, 0.0D));
/*     */         }
/* 152 */         i++;
/*     */       }
/*     */     }
/* 155 */     createList();
/* 156 */     flipUpdateUI();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void redraw()
/*     */   {
/* 165 */     int position = 0;
/*     */     
/*     */ 
/* 168 */     for (int rank = 0; rank < 8; rank++) {
/* 169 */       for (int file = 0; file < 8; file++) { int figure;
/* 170 */         int figure; if (this.flip) figure = this.hm.getFlippedFigure(rank, file); else
/* 171 */           figure = this.hm.getFigure(rank, file);
/* 172 */         JPanel panel = (JPanel)this.chessboard.getComponent(position);
/* 173 */         panel.removeAll();
/* 174 */         JLabel piece = new JLabel(this.scaledImageMap.getImage("ohaychess/images/set2/" + figure + ".png"));
/* 175 */         panel.add(piece);
/* 176 */         piece.setSize(this.scaledImageMap.getSize("ohaychess/images/set2/" + figure + ".png"), 
/* 177 */           this.scaledImageMap.getSize("ohaychess/images/set2/" + figure + ".png"));
/* 178 */         piece.setLocation(0, 0);
/* 179 */         position++;
/*     */       }
/*     */     }
/* 182 */     updateUI();
/*     */   }
/*     */   
/*     */ 
/*     */   private void createList() {}
/*     */   
/*     */ 
/*     */   private void flipUpdateUI()
/*     */   {
/* 191 */     this.constricter.add(new JLabel(new ImageIcon("ohaychess/images/grid/0.gif")), new GBC(1, 1, 1, 1).setWeight(1.0D, 0.0D).setFill(1));
/* 192 */     int xH = 2;
/* 193 */     if (this.flip) {
/* 194 */       for (int x = 1; x < 9; xH++) {
/* 195 */         this.constricter.add(new JLabel(new ImageIcon("ohaychess/images/grid/" + (9 - x) + ".png")), new GBC(1, xH, 1, 1).setWeight(0.0D, 1.0D).setFill(1));x++;
/*     */       }
/*     */       
/*     */     } else {
/* 199 */       for (int x = 1; x < 9; xH++) {
/* 200 */         this.constricter.add(new JLabel(new ImageIcon("ohaychess/images/grid/" + x + ".png")), new GBC(1, xH, 1, 1).setWeight(0.0D, 1.0D).setFill(1));x++;
/*     */       }
/*     */     }
/* 203 */     xH = 2;
/* 204 */     for (int x = 9; x < 17; xH++) {
/* 205 */       this.constricter.add(new JLabel(new ImageIcon("ohaychess/images/grid/" + x + ".png")), new GBC(xH, 1, 1, 1).setWeight(1.0D, 0.0D).setFill(1));x++;
/*     */     }
/*     */     
/* 208 */     this.constricter.add(this.chessboard, new GBC(2, 2, 8, 8).setWeight(0.0D, 0.0D));
/* 209 */     add(this.constricter, new GBC(1, 1, 1, 1).setWeight(0.0D, 0.0D));
/*     */   }
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/BoardPanel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */