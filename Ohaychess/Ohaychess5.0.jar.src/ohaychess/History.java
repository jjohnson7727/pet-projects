/*     */ package ohaychess;
/*     */ 
/*     */ import java.io.IOException;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class History
/*     */ {
/*     */   private String black;
/*     */   private String date;
/*     */   private String white;
/*     */   
/*     */   public History()
/*     */   {
/*  19 */     this.white = "";
/*  20 */     this.black = "";
/*  21 */     this.date = "";
/*  22 */     this.moveList = new ArrayList();
/*  23 */     this.position = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public History(String s)
/*     */   {
/*  32 */     this.white = "";
/*  33 */     this.black = "";
/*  34 */     this.date = "";
/*  35 */     this.moveList = new ArrayList();
/*  36 */     this.position = 0;
/*     */     try {
/*  38 */       this.parse = new Parser();
/*  39 */       this.parse.parseData(s);
/*     */     }
/*     */     catch (IOException localIOException) {}
/*  42 */     setVarable();
/*  43 */     this.lastIndex = this.moveList.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getWhite()
/*     */   {
/*  52 */     return this.white;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getBlack()
/*     */   {
/*  59 */     return this.black;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getDate()
/*     */   {
/*  66 */     return this.date;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public ArrayList<String> getHistory()
/*     */   {
/*  73 */     return this.moveList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getPosition()
/*     */   {
/*  82 */     return this.position;
/*     */   }
/*     */   
/*     */   public int getSize() {
/*  86 */     return this.lastIndex;
/*     */   }
/*     */   
/*     */   public String nextMove() {
/*  90 */     if (this.position < this.lastIndex) {
/*  91 */       return (String)this.moveList.get(this.position++);
/*     */     }
/*  93 */     return "END";
/*     */   }
/*     */   
/*     */   public String preMove()
/*     */   {
/*  98 */     if (this.position > 0) {
/*  99 */       return (String)this.moveList.get(--this.position);
/*     */     }
/* 101 */     return "END";
/*     */   }
/*     */   
/*     */   private void setVarable() {
/* 105 */     this.white = this.parse.getWhite();
/* 106 */     this.black = this.parse.getBlack();
/* 107 */     this.date = this.parse.getDate();
/* 108 */     this.moveList = this.parse.getList();
/* 109 */     this.parse = null;
/*     */   }
/*     */   
/* 112 */   public void setPosition() { this.position = 0; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 119 */   private final String END = "END";
/*     */   private ArrayList<String> moveList;
/*     */   private volatile int position;
/*     */   private volatile int lastIndex;
/*     */   private Parser parse;
/* 124 */   public String error = "";
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/History.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */