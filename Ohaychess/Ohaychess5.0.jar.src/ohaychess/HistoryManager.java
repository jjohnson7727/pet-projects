/*     */ package ohaychess;
/*     */ 
/*     */ import java.io.PrintStream;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class HistoryManager
/*     */ {
/*     */   public HistoryManager(String s)
/*     */   {
/*  22 */     setBoard();
/*  23 */     this.turnBool = false;
/*  24 */     this.endBool = false;
/*  25 */     this.promotion = new ArrayList();
/*  26 */     this.fileStart = 0;
/*  27 */     this.fileEnd = 0;
/*  28 */     this.rankStart = 0;
/*  29 */     this.rankEnd = 0;
/*  30 */     this.h = new History(s);
/*  31 */     this.grave = new Valhalla();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getFigure(int x, int y)
/*     */   {
/*  41 */     return this.workboard[x][y];
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getFlippedFigure(int x, int y)
/*     */   {
/*  51 */     return this.workboard[(this.workboard.length - x - 1)][(this.workboard.length - y - 1)];
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void nextMove()
/*     */   {
/*  59 */     String s = this.h.nextMove();
/*  60 */     if (!s.equals("END")) {
/*  61 */       if (s.equals("o-o-o")) {
/*  62 */         if (this.turnBool) {
/*  63 */           this.workboard[7][2] = this.workboard[7][4];
/*  64 */           this.workboard[7][3] = this.workboard[7][0];
/*  65 */           this.workboard[7][4] = 0;
/*  66 */           this.workboard[7][0] = 0;
/*     */         }
/*     */         else {
/*  69 */           this.workboard[0][2] = this.workboard[0][4];
/*  70 */           this.workboard[0][3] = this.workboard[0][0];
/*  71 */           this.workboard[0][4] = 0;
/*  72 */           this.workboard[0][0] = 0;
/*     */         }
/*     */       }
/*  75 */       else if (s.equals("o-o")) {
/*  76 */         if (this.turnBool) {
/*  77 */           this.workboard[7][6] = this.workboard[7][4];
/*  78 */           this.workboard[7][5] = this.workboard[7][7];
/*  79 */           this.workboard[7][4] = 0;
/*  80 */           this.workboard[7][7] = 0;
/*     */         }
/*     */         else {
/*  83 */           this.workboard[0][6] = this.workboard[0][4];
/*  84 */           this.workboard[0][5] = this.workboard[0][7];
/*  85 */           this.workboard[0][4] = 0;
/*  86 */           this.workboard[0][7] = 0;
/*     */         }
/*     */       }
/*     */       else {
/*  90 */         String[] move = s.split("[x-]");
/*  91 */         this.fileStart = findKey(move[0].charAt(0), this.file);
/*  92 */         this.fileEnd = findKey(move[1].charAt(0), this.file);
/*  93 */         this.rankStart = findKey(move[0].charAt(1), this.rank);
/*  94 */         this.rankEnd = findKey(move[1].charAt(1), this.rank);
/*  95 */         if (s.charAt(2) == 'x') {
/*  96 */           this.grave.toOdin(s.hashCode() - (this.rankEnd + this.fileEnd), this.workboard[this.rankEnd][this.fileEnd]);
/*     */         }
/*  98 */         this.workboard[this.rankEnd][this.fileEnd] = this.workboard[this.rankStart][this.fileStart];
/*  99 */         this.workboard[this.rankStart][this.fileStart] = 0;
/*     */         
/*     */ 
/* 102 */         if (this.turnBool) {
/* 103 */           if ((piece.BPAWN.ordinal() == this.workboard[this.rankEnd][this.fileEnd]) && 
/* 104 */             (this.rankEnd == 0)) {
/* 105 */             this.workboard[this.rankEnd][this.fileEnd] = piece.BPPRO.ordinal();
/* 106 */             this.promotion.add(Integer.valueOf(this.h.getPosition() - 1));
/*     */           }
/*     */         }
/* 109 */         else if ((piece.WPAWN.ordinal() == this.workboard[this.rankEnd][this.fileEnd]) && 
/* 110 */           (this.rankEnd == 7)) {
/* 111 */           this.workboard[this.rankEnd][this.fileEnd] = piece.WPPRO.ordinal();
/* 112 */           this.promotion.add(Integer.valueOf(this.h.getPosition() - 1));
/*     */         }
/* 114 */         reset();
/*     */       }
/* 116 */       this.turnBool = (!this.turnBool);
/*     */     }
/*     */     else {
/* 119 */       this.endBool = true;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void preMove()
/*     */   {
/* 130 */     this.endBool = false;
/* 131 */     String s = this.h.preMove();
/* 132 */     if (!s.equals("END")) {
/* 133 */       this.turnBool = (!this.turnBool);
/* 134 */       if (s.equals("o-o-o")) {
/* 135 */         if (this.turnBool) {
/* 136 */           this.workboard[7][4] = this.workboard[7][2];
/* 137 */           this.workboard[7][0] = this.workboard[7][3];
/* 138 */           this.workboard[7][2] = 0;
/* 139 */           this.workboard[7][3] = 0;
/*     */         }
/*     */         else {
/* 142 */           this.workboard[0][4] = this.workboard[0][2];
/* 143 */           this.workboard[0][0] = this.workboard[0][3];
/* 144 */           this.workboard[0][2] = 0;
/* 145 */           this.workboard[0][3] = 0;
/*     */         }
/*     */       }
/* 148 */       else if (s.equals("o-o")) {
/* 149 */         if (this.turnBool) {
/* 150 */           this.workboard[7][4] = this.workboard[7][6];
/* 151 */           this.workboard[7][7] = this.workboard[7][5];
/* 152 */           this.workboard[7][6] = 0;
/* 153 */           this.workboard[7][5] = 0;
/*     */         }
/*     */         else {
/* 156 */           this.workboard[0][4] = this.workboard[0][6];
/* 157 */           this.workboard[0][7] = this.workboard[0][5];
/* 158 */           this.workboard[0][6] = 0;
/* 159 */           this.workboard[0][5] = 0;
/*     */         }
/*     */       }
/*     */       else {
/* 163 */         String[] move = s.split("[x-]");
/* 164 */         this.fileStart = findKey(move[0].charAt(0), this.file);
/* 165 */         this.rankStart = findKey(move[0].charAt(1), this.rank);
/* 166 */         this.fileEnd = findKey(move[1].charAt(0), this.file);
/* 167 */         this.rankEnd = findKey(move[1].charAt(1), this.rank);
/* 168 */         this.workboard[this.rankStart][this.fileStart] = this.workboard[this.rankEnd][this.fileEnd];
/* 169 */         if (s.charAt(2) == 'x') {
/* 170 */           this.workboard[this.rankEnd][this.fileEnd] = this.grave.resurrect(s.hashCode() - (this.rankEnd + this.fileEnd));
/*     */         }
/*     */         else {
/* 173 */           this.workboard[this.rankEnd][this.fileEnd] = 0;
/*     */         }
/*     */         
/* 176 */         if ((!this.promotion.isEmpty()) && (((Integer)this.promotion.get(this.promotion.size() - 1)).intValue() == this.h.getPosition())) {
/* 177 */           int x = this.workboard[this.rankStart][this.fileStart];
/* 178 */           if (x == 14) {
/* 179 */             this.workboard[this.rankStart][this.fileStart] = 6;
/* 180 */             this.promotion.remove(this.promotion.size() - 1);
/*     */           }
/* 182 */           else if (x == 13) {
/* 183 */             this.workboard[this.rankStart][this.fileStart] = 12;
/* 184 */             this.promotion.remove(this.promotion.size() - 1);
/*     */           }
/*     */         }
/* 187 */         reset();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private int findKey(char cHar, char[] array)
/*     */   {
/* 201 */     for (int x = 0; x < 8; x++) {
/* 202 */       if (array[x] == cHar) {
/* 203 */         return x;
/*     */       }
/*     */     }
/* 206 */     System.out.println("Cannot find KEY!");
/* 207 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void reset()
/*     */   {
/* 222 */     this.fileStart = 0;
/* 223 */     this.fileEnd = 0;
/* 224 */     this.rankStart = 0;
/* 225 */     this.rankEnd = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private void setBoard()
/*     */   {
/* 233 */     this.workboard = new int[][] {
/* 234 */       { piece.WROOK.ordinal(), piece.WKNIGHT.ordinal(), piece.WBISHOP.ordinal(), piece.WQUEEN.ordinal(), piece.WKING.ordinal(), piece.WBISHOP.ordinal(), piece.WKNIGHT.ordinal(), piece.WROOK.ordinal() }, 
/* 235 */       { piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal(), piece.WPAWN.ordinal() }, 
/* 236 */       { piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal() }, 
/* 237 */       { piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal() }, 
/* 238 */       { piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal() }, 
/* 239 */       { piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal(), piece.EMPTY.ordinal() }, 
/* 240 */       { piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal(), piece.BPAWN.ordinal() }, 
/* 241 */       { piece.BROOK.ordinal(), piece.BKNIGHT.ordinal(), piece.BBISHOP.ordinal(), piece.BQUEEN.ordinal(), piece.BKING.ordinal(), piece.BBISHOP.ordinal(), piece.BKNIGHT.ordinal(), piece.BROOK.ordinal() } };
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void traverseEnd()
/*     */   {
/* 260 */     while (!this.endBool) {
/* 261 */       nextMove();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void traverseStart()
/*     */   {
/* 270 */     this.endBool = false;
/* 271 */     this.h.setPosition();
/* 272 */     setBoard();
/* 273 */     this.grave.emptyGrave();
/* 274 */     this.turnBool = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 298 */   private final String PATTERN = "[x-]";
/*     */   
/*     */   private boolean endBool;
/*     */   
/*     */   private boolean turnBool;
/*     */   
/*     */   private int fileStart;
/*     */   
/*     */   private int fileEnd;
/*     */   private int rankStart;
/*     */   private int rankEnd;
/* 309 */   private char[] file = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
/* 310 */   private char[] rank = { '1', '2', '3', '4', '5', '6', '7', '8' };
/*     */   private volatile int[][] workboard;
/*     */   private ArrayList<Integer> promotion;
/*     */   
/* 314 */   private static enum piece { EMPTY,  WKING,  WQUEEN,  WROOK,  WKNIGHT,  WBISHOP,  WPAWN,  BKING,  BQUEEN,  BROOK,  BKNIGHT,  BBISHOP,  BPAWN,  BPPRO,  WPPRO;
/*     */   }
/*     */   
/*     */   private Valhalla grave;
/*     */   private History h;
/*     */ }


/* Location:              /Users/jessejohnson/Desktop/Ohaychess5.0.jar!/ohaychess/HistoryManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */