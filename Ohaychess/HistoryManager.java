package ohaychess;
/**
 *@(#)HistoryManager.java 8/20/2006
 *@version 1.1 8/29/2006
 *@author Jesse A. Johnson
 */

/**
 * <pre>HistoryManager - A class for holding the current game board setup.
 * Reads in the next move from History and translates it to its number
 * representation for array storage.</pre>
 *
 * <i>Part of Alpha Project Unus for The Java Alpha Team.</i>
 */
public class HistoryManager {

	/**
	 * Constructor to create new manager.
	 * A new HistoryManager object.
	 */
	public HistoryManager() {
		setBoard();
		turnBool = false;
		endBool = false;
		h = new History("test.txt");
		grave = new Valhalla();
		fileStart = 0;
		fileEnd = 0;
		rankStart = 0;
		rankEnd = 0;
	}
	
	/**
	 * Method to reanimate dead player.
	 * @param x The rank of the figure.
	 * @param y The file of the figure.
	 * @return int figure id.
	 */
	public int getFigure(int x, int y) {
		return boardLayout[x][y];
	}
	
	/**
	 * Method to retrieve next move, parse it and 
	 * translate the new boardLayout.
	 */
	public void nextMove() {
		String s = h.nextMove();
		if(!s.equals("END")) {
			if(s.equals("o-o-o")) {//CASTLE QUEEN SIDE 
				if(turnBool){
					boardLayout[7][2] = boardLayout[7][4];
					boardLayout[7][3] = boardLayout[7][0];//BLACK
					boardLayout[7][4] = 0;
					boardLayout[7][0] = 0;
				}
				else { 
					boardLayout[0][2] = boardLayout[0][4];
					boardLayout[0][3] = boardLayout[0][0];//WHITE
					boardLayout[0][4] = 0;
					boardLayout[0][0] = 0;
				}
			}
			else if(s.equals("o-o")) {//CASTLE KING SIDE
				if(turnBool){
					boardLayout[7][6] = boardLayout[7][4];
					boardLayout[7][5] = boardLayout[7][7];//BLACK
					boardLayout[7][4] = 0;
					boardLayout[7][7] = 0;
				}
				else {
					boardLayout[0][6] = boardLayout[0][4];
					boardLayout[0][5] = boardLayout[0][7];//WHITE
					boardLayout[0][4] = 0;
					boardLayout[0][7] = 0;
				}
			}
			else {
				String [] move = s.split(PATTERN);
				fileStart = findKey( move[0].charAt(0), file);
				fileEnd = findKey(move[1].charAt(0), file);
				rankStart = findKey(move[0].charAt(1), rank);
				rankEnd = findKey(move[1].charAt(1), rank);
				if(s.charAt(2)== 'x') {
					grave.toOdin( s.hashCode() - (rankEnd+fileEnd), boardLayout[rankEnd][fileEnd] );
				}
				boardLayout[rankEnd][fileEnd] = boardLayout[rankStart][fileStart];
				boardLayout[rankStart][fileStart] = 0;
				reset();
			}
			turnBool = !turnBool;
		}
		else {
			endBool = true;
		}
	}
	
	/**
	 * Method to retrieve previous move, parse it and 
	 * translate the new boardLayout.
	 */
	public void preMove() {
		endBool = false;
		String s = h.preMove();
		if(!s.equals("END")) {
			turnBool = !turnBool;
			if(s.equals("o-o-o")) {
				if(turnBool){
					boardLayout[7][4] = boardLayout[7][2];
					boardLayout[7][0] = boardLayout[7][3];//BLACK					
					boardLayout[7][2] = 0;
					boardLayout[7][3] = 0;
				}
				else { 
					boardLayout[0][4] = boardLayout[0][2];
					boardLayout[0][0] = boardLayout[0][3];//WHITE
					boardLayout[0][2] = 0;
					boardLayout[0][3] = 0;
				}
			}
			else if(s.equals("o-o")) {
				if(turnBool){//CASTLE KING SIDE 
					boardLayout[7][4] = boardLayout[7][6];
					boardLayout[7][7] = boardLayout[7][5];//BLACK
					boardLayout[7][6] = 0;
					boardLayout[7][5] = 0;
				}
				else {
					boardLayout[0][4] = boardLayout[0][6];
					boardLayout[0][7] = boardLayout[0][5];//WHITE
					boardLayout[0][6] = 0;
					boardLayout[0][5] = 0;
				}
			}
			else {
				String [] move = s.split(PATTERN);
				fileStart = findKey( move[0].charAt(0), file);
				rankStart = findKey(move[0].charAt(1), rank);
				fileEnd = findKey(move[1].charAt(0), file);
				rankEnd = findKey(move[1].charAt(1), rank);
				boardLayout[rankStart][fileStart] = boardLayout[rankEnd][fileEnd];
				if(s.charAt(2)== 'x') {
					boardLayout[rankEnd][fileEnd] = grave.resurrect( s.hashCode()- (rankEnd+fileEnd) );
				}
				else {
					boardLayout[rankEnd][fileEnd] = 0;
				}
				reset();
			}
		}
	}
	
	/**
	 * Method to find integer equivalent from parsed move.
	 * @param cHar equivalent to find.
	 * @param array The collection to search, either rank or file
	 * @return int The equivalent, the key.
	 */
	private int findKey(char cHar, char[] array) {
		for(int x = 0; x<8;x++) {
			if(array[x] == cHar) {
				return x;
			}
		}
		System.out.println("Cannot find KEY!");
		return -1;
	}
	
	/**
	 * Method to ready variables for nextMore or preMove call.
	 */
	private void reset() {
		fileStart = 0;
		fileEnd = 0;
		rankStart = 0;
		rankEnd = 0;
	}
	
	/**
	 * Method to set figure ids to their starting
	 * positions.
	 */
	private void setBoard() {
		boardLayout = new int [][] {			//WHITE SIDE																												//WHITE SQUARE h RANK
				{piece.WROOK.ordinal(),piece.WKNIGHT.ordinal(),piece.WBISHOP.ordinal(),piece.WQUEEN.ordinal(),piece.WKING.ordinal(),piece.WBISHOP.ordinal(),piece.WKNIGHT.ordinal(),piece.WROOK.ordinal()},
				{piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal(),piece.WPAWN.ordinal()},
				{piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal()},
				{piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal()},
				{piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal()},
				{piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal(),piece.EMPTY.ordinal()},
				{piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal(),piece.BPAWN.ordinal()},
				{piece.BROOK.ordinal(),piece.BKNIGHT.ordinal(),piece.BBISHOP.ordinal(),piece.BQUEEN.ordinal(),piece.BKING.ordinal(),piece.BBISHOP.ordinal(),piece.BKNIGHT.ordinal(),piece.BROOK.ordinal()},			
			};								//BLACK SIDE
	}
	
	/**
	 * Method to iterate to the end of 
	 * the history list.
	 */
	public void traverseEnd() {
		while(endBool==false) {
			nextMove();
		}
	}
	
	/**
	 * Method to reset boardLayout to starting
	 * position along with other esential objects.
	 */
	public void traverseStart() {
		endBool = false;
		h.setPosition();
		setBoard();
		grave.emptyGrave();
		turnBool = false;
	}
	
//	---------------------------------------VARIABLES
	private final String PATTERN = "[x-]";
	
	private boolean endBool;
	private boolean turnBool;// dictate what color to castle as 0-0 is ambigious

	private int fileStart;
	private int fileEnd;
	private int rankStart;
	private int rankEnd;
	
	private char []file = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	private char []rank = {'1', '2', '3', '4', '5', '6', '7', '8'};

	private static volatile int[][] boardLayout;
	private enum piece {EMPTY, WKING, WQUEEN, WROOK, WKNIGHT, WBISHOP, WPAWN, BKING, BQUEEN, BROOK, BKNIGHT, BBISHOP, BPAWN};

	private Valhalla grave;
	private History h;
}