/*struct Wheel {
  int en;
  int alpha;
  int beta;
};

Wheel _wheel_LEFT = { 5, 7, 6 };
Wheel _wheel_RIGHT = { 10, 9, 8 };

void wheel_init() {
  pinMode(_wheel_RIGHT.alpha, OUTPUT);
  pinMode(_wheel_RIGHT.beta, OUTPUT);
  pinMode(_wheel_RIGHT.en, OUTPUT);

  pinMode(_wheel_LEFT.alpha, OUTPUT);
  pinMode(_wheel_LEFT.beta, OUTPUT);
  pinMode(_wheel_LEFT.en, OUTPUT);
  
  digitalWrite(_wheel_RIGHT.en, HIGH);
  digitalWrite(_wheel_LEFT.en, HIGH);
}

void _wheel_process(Wheel wheel, int alphaVoltage, int betaVoltage) {
  digitalWrite(wheel.alpha, alphaVoltage);
  digitalWrite(wheel.beta, betaVoltage);
}

void _wheel_forward(Wheel wheel) { _wheel_process(wheel, LOW, HIGH); }
void _wheel_stop(Wheel wheel) { _wheel_process(wheel, LOW, LOW); }
void _wheel_backward(Wheel wheel) { _wheel_process(wheel, HIGH, LOW); }

void wheel_forward() {
  _wheel_forward(_wheel_LEFT);
  _wheel_forward(_wheel_RIGHT);
}

void wheel_stop() {
  _wheel_stop(_wheel_LEFT);
  _wheel_stop(_wheel_RIGHT);
}

void wheel_backward() {
  _wheel_backward(_wheel_LEFT);
  _wheel_backward(_wheel_RIGHT);
}

void wheel_turnRight() {
  _wheel_forward(_wheel_LEFT);
  _wheel_backward(_wheel_RIGHT);
}

void wheel_turnLeft() {
  _wheel_forward(_wheel_RIGHT);
  _wheel_backward(_wheel_LEFT);
}
*/