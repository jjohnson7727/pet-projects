int _blutooth_LED = 13;
volatile int _blutooth_state = LOW;
volatile boolean _blutooth_running = false;
char _blutooth_command = 'N';

void blutooth_init() {
  pinMode(_blutooth_LED, OUTPUT);
  Serial.begin(9600);
  Serial.println("Online");
}

void blutooth_stateChanged() {
  if(_blutooth_command == 'A') {
    _blutooth_state = HIGH;
    Serial.println("A HIGH");
  } else if(_blutooth_command == 'a') {
    _blutooth_state = LOW;
    Serial.println("a LOW");
  }

  digitalWrite(_blutooth_LED, _blutooth_state);
}

void blutooth_recieveCommand() {
  char getstr = Serial.read();
  if(_blutooth_command != getstr) {
    _blutooth_command = getstr;
    blutooth_stateChanged();
  }
}

void blutooth_recieve() {
  if(false == _blutooth_running) {
    _blutooth_running = true;
    blutooth_recieveCommand();
    _blutooth_running = false;
  }
}
