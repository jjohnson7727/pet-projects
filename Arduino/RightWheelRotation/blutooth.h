#include "_blutooth.h"

void blutooth_init() {
  _blutooth_process(blutooth_action_instanceOf, NULL);
}

void blutooth_subscrib(void (*fp)(void)) {
  _blutooth_process(blutooth_action_subscribe, fp);
}

