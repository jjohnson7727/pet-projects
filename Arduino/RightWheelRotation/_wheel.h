enum wheel_action {
  wheel_action_forward,
  wheel_action_backward,
  wheel_action_halt,
  wheel_action_left,
  wheel_action_right,
  wheel_action_instanceOf
};

struct _wheel_actuator {
  int en;
  int alpha;
  int beta;
};

struct _wheel {
  _wheel_actuator left = {5, 7, 6};
  _wheel_actuator right = {10, 9, 8};

  void init() {
    Serial.println("WheelActuatorOnline");
  }

  void forward() {
    //process(left, LOW, HIGH);
    //process(right, LOW, HIGH);
    Serial.println("Wheel move forward");
  }

  //void process(_wheel_actuator actuator, int alpha, int beta) {
    //digitalWrite(actuator.alpha, alpha);
    //digitalWrite(actuator.beta, beta);
  //}
};

void _wheel_process(wheel_action action) {
  static _wheel model = {};

  switch (action) {
    case wheel_action_instanceOf:
      model.init();
    break;
    case wheel_action_forward:
      model.forward();
    break;
    default:
      Serial.println("Unimplemented");
    break;
  }
}

