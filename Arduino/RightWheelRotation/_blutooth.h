#include "uthash.h"

enum blutooth_action {
  blutooth_action_subscribe,
  blutooth_action_instanceOf
};

struct _blutooth {
  int numberOfSubscribers = -1;
  void (*subscribers[])(void);
  
  void init() {
    Serial.begin(9600);
    Serial.println("BlutoothOnline");
    delay(200);
  }

  void subscribe(void (*fp)(void)) {
    numberOfSubscribers++;
    subscribers[numberOfSubscribers] = fp;
  }
};

void _blutooth_process(blutooth_action action, void (*fp)()) {
  static _blutooth model = {};
  
  switch (action) {
    case blutooth_action_instanceOf:
      model.init();
    break;
    case blutooth_action_subscribe:
      model.subscribe(fp);
    break;
  }
}
