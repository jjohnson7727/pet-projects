#include "_wheel.h"

void wheel_init() {
  _wheel_process(wheel_action_instanceOf);
}

void wheel_forward() {
  _wheel_process(wheel_action_forward);
}

void wheel_backward() {
  _wheel_process(wheel_action_backward);
}

void wheel_stop() {
  _wheel_process(wheel_action_halt);
}

void wheel_left() {
  _wheel_process(wheel_action_left);
}

void wheel_right() {
  _wheel_process(wheel_action_right);
}

