#include "remoteVehicle.h"

void setup() {
  remoteVehicle_init();
}

void loop() {
  remoteVehicle_listen();
}

/*
void run() {
  Serial.println("Forward");
  wheel_forward();
  delay(250);

  Serial.println("Stop");
  wheel_stop();
  delay(250);

  Serial.println("Backward");
  wheel_backward();
  delay(250);

  Serial.println("Stop");
  wheel_stop();
  delay(250);

  Serial.println("Right turn");
  wheel_turnRight();
  delay(500);

  Serial.println("Left turn");
  wheel_turnLeft();
  delay(500);

  Serial.println("Rught turn");
  wheel_turnRight();
  delay(500);

  Serial.println("Stop");
  wheel_stop();
}
*/
