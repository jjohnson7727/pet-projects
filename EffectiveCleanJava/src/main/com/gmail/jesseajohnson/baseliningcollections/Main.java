package com.gmail.jesseajohnson.baseliningcollections;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Consumer;

public class Main {

    private static final int DEFAULT = 10_000;
    private final List<Double> numbers;

    private static final Predicate<Double> PRIME = p -> {
        boolean result = true;
        for(double x = 2.0; x < p; x++) {
            if(p/x % 1 == 0) {
                result = false;
                break;
            }
        }
        return result;
    };

    public static void main(String[] args) {
        int bound = parseArgs(args);
        Main executor = new Main(bound);
        
        timeOperation(executor, Main::invokeLegacyForLoopCollectionProcess);
        timeOperation(executor, Main::invokeEnhancedForLoopCollectionProcess);
        timeOperation(executor, Main::invokeSerialStreamProcess);
        timeOperation(executor, Main::invokeParallelStreamProcess);
    }

    private static void timeOperation(Main object, Consumer<Main> operation) {
        long start = System.currentTimeMillis();
        operation.accept(object);
        long stop = System.currentTimeMillis();
        printTime(start, stop);
    }

    private static void printTime(long start, long stop) {
        double time = stop - start;
        String postfix;
        if(time > 999) {
            time = time / 1000;
            postfix = " seconds.";
        } else {
            postfix = " milliseconds.";
        }
        System.out.println(" took " + time + postfix);
    }

    private static final int parseArgs(String[] args) {
        int bound = 0;
        
        if(args.length == 0) {
            bound = DEFAULT;
        } else {
            try {
                bound = Integer.parseInt(args[0]);    
            } catch (NumberFormatException e) {
                System.out.println("Please enter an integer bound to process or leave blank.");
                System.exit(0);
            }
        }

        if(bound < 3) {
            System.out.println("Please enter a bound greater than 2 or leave blank.");
            System.exit(0);
        }

        return bound;
    }

    public Main(int max) {
        numbers = new ArrayList<>();
        for (int i = 0; i <= max; i++)
            numbers.add(new Double(i));
    }

    public void invokeLegacyForLoopCollectionProcess() {
        long sum = 0;
        double value;
        for(int x = 0; x < numbers.size(); x++) {
            value = numbers.get(x);
            sum += PRIME.test(value) ? value : 0;
        }
        System.out.print("Legacy for loop   sum: " + sum);
    }

    public void invokeEnhancedForLoopCollectionProcess() {
        long sum = 0;
        for(Double value : numbers) {
            sum += PRIME.test(value) ? value : 0;
        }
        System.out.print("Enhanced for loop sum: " + sum);
    }

    public void invokeSerialStreamProcess() {
        long sum = numbers.stream().filter(PRIME).mapToLong(Double::longValue).sum();
        System.out.print("Serial stream     sum: " + sum);
    }

    public void invokeParallelStreamProcess() {
        long sum = numbers.parallelStream().filter(PRIME).mapToLong(Double::longValue).sum();
        System.out.print("Parallel stream   sum: " + sum);
    }
}