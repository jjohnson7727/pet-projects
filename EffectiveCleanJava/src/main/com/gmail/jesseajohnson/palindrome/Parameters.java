package com.gmail.jesseajohnson.palindrome;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.cli.CommandLine;

import com.gmail.jesseajohnson.tool.CommandLineFactory;

public class Parameters {
  
  private final String FIRST = "first";
  private final String SECOND = "second";
  private final CommandLine command;
  
  public Parameters(String[] args) {
    this.command = CommandLineFactory.build(args);
  }

  public Date getFirst() {
    return tryParsingWith(FIRST);
  }

  public Date getSecond() {
    return tryParsingWith(SECOND);
  }

  private Date tryParsingWith(String value) {
    try {
        return parseDate(value);
    } catch(java.text.ParseException e) {
        throw new DateOptionFormatException();
    }
  }

private Date parseDate(String value) throws java.text.ParseException {
    return new SimpleDateFormat("MM/dd/yyyy").parse(command.getOptionValue(value));
}

@SuppressWarnings("serial")
private class DateOptionFormatException extends RuntimeException {}
}
