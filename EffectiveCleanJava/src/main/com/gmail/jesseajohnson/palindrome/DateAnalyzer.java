package com.gmail.jesseajohnson.palindrome;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.gmail.jesseajohnson.tool.Analyzer;
import com.gmail.jesseajohnson.tool.ObjToBooleanFunction;

public class DateAnalyzer extends Analyzer<Date> {

  public static final DateFormat FORMATTER = new SimpleDateFormat("MMddyyyy");

  private DateAnalyzer(Date subject) {
    super(subject);
  }
  
  public static ObjToBooleanFunction<Date> solutionFunction() {
    return (subject) -> {
      return make(subject);
    };
  }
  
  public static boolean make(Date subject) {
    return new DateAnalyzer(subject).analyze();
  }

  @Override
  public boolean analyze() {
    String value = FORMATTER.format(subject);
    return new StringAnalyzer(value).analyze();
  }
}