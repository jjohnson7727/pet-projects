package com.gmail.jesseajohnson.palindrome;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.function.Supplier;

public class Printer {

    public static final DateFormat DATEWRITER = new SimpleDateFormat("MM/dd/yyyy");

    public static void printWith(Supplier<Collection<Date>> supplier) {
        outputIntroduction();
        outputResults(supplier.get());
        outputConculsion();
    }

    private static void outputIntroduction() {
        String output = "/*******************************************\n";
        output       += " *                                         *\n";
        output       += " *      Palindrome Detector Challenge      *\n";
        output       += " *                                         *\n";
        output       += " ******************************************/\n\n";
        output += "--> What: Find all palindromes in the given date range from each date when formatted as MMddyyyy.\n";
        output += "--> Fact: There will be 12 Palindrome Days in the 21st century in the mm-dd-yyyy format.\n";
        output += "          The first one was on October 2, 2001 (10-02-2001) and the last one will be on September 2, 2090 (09-02-2090).\n";
        output += "\n|-------------------------------------";
        
        System.out.print(output);
    }

    private static void outputResults(Collection<Date> set) {
        String output = " We found: " + set.size() + " palindrome(s).\n";
        
        for(Date date : set) {
            output += "|     " + DATEWRITER.format(date) + "\n";
        }

        output += "|------------------------------------";
        output += " Java version: " + System.getProperty("java.version");
        System.out.println(output);
    }

    private static void outputConculsion() {}
}
