package com.gmail.jesseajohnson.palindrome;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OptionsFactory {

    private static final String FIRST = "f";
    private static final String SECOND = "s";
    
    private OptionsFactory() {}

    public static Options instanceOf() {
      Options options = new Options();

      options.addOption(makeFirstOption());
      options.addOption(makeSecondOption());

      return options;
    }

    private static Option makeFirstOption() {
        return Option.builder(FIRST)
                .longOpt("first")
                .required()
                .hasArg()
                .optionalArg(false)
                .type(Date.class)
                .argName("firstDate")
                .desc("Sets the first date. MM/dd/yyyy")
                .build();
    }

    private static Option makeSecondOption() {
        return Option.builder(SECOND)
                .longOpt("second")
                .required()
                .hasArg()
                .optionalArg(false)
                .type(Date.class)
                .argName("secondDate")
                .desc("Sets the second date. MM/dd/yyyy")
                .build();
    }

    public Date getSecondDate(CommandLine seed) {
        return tryParsingWith(seed, SECOND);
    }

    public Date getFirstDate(CommandLine seed) {
        return tryParsingWith(seed, FIRST);
    }

    private Date tryParsingWith(CommandLine seed, String value) {
        try {
            return parseDate(seed, value);
        } catch(java.text.ParseException e) {
            throw new DateOptionFormatException();
        }
    }

    private Date parseDate(CommandLine seed, String value) throws java.text.ParseException {
        return new SimpleDateFormat("MM/dd/yyyy").parse(seed.getOptionValue(value));
    }

    @SuppressWarnings("serial")
    private class DateOptionFormatException extends RuntimeException {}
}