package com.gmail.jesseajohnson.palindrome;

import com.gmail.jesseajohnson.tool.Analyzer;

public class StringAnalyzer extends Analyzer<String> {

  public StringAnalyzer(String subject) {
    super(subject);
  }
  
  public static boolean apply(String subject) {
    return new StringAnalyzer(subject).analyze();
  }
  
  @Override
  public boolean analyze() {
    if (!hasNext())
      return true;
    else if (doCharactersEquate()) {
      return new StringAnalyzer(next()).analyze();
    }
    return false;
  }

  boolean doCharactersEquate() {
    return subject.charAt(0) == subject.charAt(subject.length() - 1);
  }

  public boolean hasNext() {
    return subject.length() > 1;
  }

  public String next() {
    return subject.substring(1, subject.length() - 1);
  }
}