 package com.gmail.jesseajohnson.palindrome;

import java.util.Date;

import com.gmail.jesseajohnson.tool.Chronological;
import com.gmail.jesseajohnson.tool.Solution;

public class Main {
  
  private final Parameters parameters;

  private Main(String[] args) {
    this.parameters = new Parameters(args);
  }

  public static void main(String[] args) {
    new Main(args).run();
  }
  
  public void run() {
    Printer.printWith(() -> instanceOfChronologicalSolution().find());
  }

  private Solution<Date> instanceOfChronologicalSolution() {
    Chronological c = Chronological.instanceWith(parameters.getFirst(), parameters.getSecond());
    return new Solution<Date>(c, DateAnalyzer.solutionFunction());
  }
}
