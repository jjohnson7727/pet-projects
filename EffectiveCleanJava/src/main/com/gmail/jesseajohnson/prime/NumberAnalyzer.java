package com.gmail.jesseajohnson.prime;

import com.gmail.jesseajohnson.tool.Analyzer;

public class NumberAnalyzer extends Analyzer<Double> {

	public NumberAnalyzer(Double subject) {
		super(subject);
	}

	@Override
	public boolean analyze() {
		boolean result = true;

		for(double divisor = 2.0; result == true && divisor < subject; divisor++) {
			if(subject/divisor % 1 == 0) result = false;
		}
		
		return result;
	}
}
