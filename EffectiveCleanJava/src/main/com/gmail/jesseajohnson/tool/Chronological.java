package com.gmail.jesseajohnson.tool;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class Chronological implements Iterator<Date> {

  private final Calendar timekeeper;
  private final Date end;
  
  private Chronological(Date first, Date second) {
    this.end = second;
    timekeeper = Calendar.getInstance();
    timekeeper.setTime(first);
    timekeeper.add(Calendar.DAY_OF_MONTH, -1);
  }

  public static Chronological instanceWith(Date start, Date end) {
    if (datesHaveNaturalOrdering(start, end))
      return new Chronological(start, end);
    else
      return new Chronological(end, start);
  }

  private static boolean datesHaveNaturalOrdering(Date start, Date end) {
    return start.equals(end) || start.before(end);
  }
  
  @Override
  public boolean hasNext() {
    return timekeeper.getTime().before(end) || !timekeeper.getTime().equals(end);
  }

  @Override
  public Date next() {
    timekeeper.add(Calendar.DAY_OF_MONTH, 1);
    return timekeeper.getTime();
  }
}
