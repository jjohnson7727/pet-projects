package com.gmail.jesseajohnson.tool;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.gmail.jesseajohnson.palindrome.OptionsFactory;

public class CommandLineFactory {
  
  private String[] args;
  
  private CommandLineFactory(String[] args) {
    this.args = args;
  }
  
  public static CommandLine build(String[] args) {
    return new CommandLineFactory(args).make();
  }
  
  private CommandLine make() {
    try {
      return parse();
    } catch (ParseException e) {
      handleError(e);
      throw new ArgsParseException(e);
    }
  }
  
  private CommandLine parse() throws ParseException {
    Options options = OptionsFactory.instanceOf();
    return new DefaultParser().parse(options, args);
  }
  
  private void handleError(ParseException e) {
    if (e instanceof MissingArgumentException)
      printHelpMessageWithMissingArgument(e);
    else printHelpMessageWithError(e);
  }
  
  private void printHelpMessageWithMissingArgument(ParseException e) {
    MissingArgumentException m = (MissingArgumentException) e;
    new HelpFormatter().printHelp(m.getOption().getOpt(), OptionsFactory.instanceOf());
  }
  
  private void printHelpMessageWithError(ParseException e) {
    new HelpFormatter().printHelp(e.getMessage(), OptionsFactory.instanceOf());
  }
  
  @SuppressWarnings("serial")
  public class ArgsParseException extends RuntimeException {
    public ArgsParseException(Throwable t) {
      super(args.toString(), t);
    }
  }
}
