package com.gmail.jesseajohnson.tool;

@FunctionalInterface
public interface ObjToBooleanFunction<T> {

    boolean applyAsBoolean(T t);
}