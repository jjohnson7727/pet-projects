package com.gmail.jesseajohnson.tool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Solution<T> {

  private final Iterator<T> iterator;
  private final ObjToBooleanFunction<T> function;
  private final Collection<T> results;

  public Solution(Iterator<T> iterator, ObjToBooleanFunction<T> function) {
    this.iterator = iterator;
    this.function = function;
    results = new ArrayList<>();
  }

  public Collection<T> find() {
    run();
    return results;
  }

  private void run() {
    iterator.forEachRemaining((subject) -> {
      if (isConfirmed(subject))
        results.add(subject);
    }); 
  }

  private boolean isConfirmed(T subject) {
    return function.applyAsBoolean(subject);
  }
}