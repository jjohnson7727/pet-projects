package com.gmail.jesseajohnson.tool;

public abstract class Analyzer<T> {
  
  protected final T subject;
  
  public Analyzer(T subject) {
    this.subject = subject;
  }
  
  public abstract boolean analyze();
}
