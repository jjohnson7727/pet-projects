package com.gmail.jesseajohnson.tool;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.gmail.jesseajohnson.tool.ObjToBooleanFunction;
import com.gmail.jesseajohnson.tool.Solution;

public class SolutionTest {
  
	@Test
	public void find_happyPath_threePalindromesFound() throws Exception {
	  String CONTROL = StringUtils.EMPTY;
	  Solution<String> s = makeSolution(makeIterator());
	  Collection<String> actual = s.find();
	  assertEquals(3, actual.size());
	  Assert.assertFalse(actual.contains(CONTROL));
	}
	
	private Solution<String> makeSolution(Iterator<String> iterator) {
    return new Solution<String>(iterator, makeFunction());
	}
	
	private ObjToBooleanFunction<String> makeFunction() {
	  return (subject) -> {
	    return StringUtils.isNotBlank(subject);
	  };
	}
	
	private Iterator<String> makeIterator() {
	  Iterator<String> iterator = Arrays.asList("red", null, "", "green", "blue").iterator();
	  return iterator;
	}
}