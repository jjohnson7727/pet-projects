package com.gmail.jesseajohnson.palindrome;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.gmail.jesseajohnson.tool.Analyzer;

public class StringAnalyzerTest {
  
  private Analyzer<String> makeAnalyzer(String subject) {
    return new StringAnalyzer(subject);
  }
  
  @Test
  public void isPalindrome_blank_true() {
    Analyzer<String> analyzer = makeAnalyzer("");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_singleCharacter_true() {
    Analyzer<String> analyzer = makeAnalyzer("A");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_duelDifferingCharaters_false() {
    Analyzer<String> analyzer = makeAnalyzer("AB");
    assertFalse(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_duelIdenticalCharacters_true() {
    Analyzer<String> analyzer = makeAnalyzer("AA");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_identicalCharactersMiddleSpace_True() {
    Analyzer<String> analyzer = makeAnalyzer("A A");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_charactersWithSuffixSpace_false() {
    Analyzer<String> analyzer = makeAnalyzer("A A ");
    assertFalse(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_charactersWithPrefixSpace_false() {
    Analyzer<String> analyzer = makeAnalyzer(" A A");
    assertFalse(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_identicalCharactersWithPadding_true() {
    Analyzer<String> analyzer = makeAnalyzer(" A A ");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_randomNumerics_false() {
    Analyzer<String> analyzer = makeAnalyzer("10012001");
    assertFalse(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_numericMirrorHalfsOne_true() {
    Analyzer<String> analyzer = makeAnalyzer("10000001");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_numericMirrorHalfsTwo_true() {
    Analyzer<String> analyzer = makeAnalyzer("10022001");
    assertTrue(analyzer.analyze());
  }
  
  @Test
  public void isPalindrome_numericMirrorHalfsThree_true() {
    Analyzer<String> analyzer = makeAnalyzer("23455432");
    assertTrue(analyzer.analyze());
  }
}