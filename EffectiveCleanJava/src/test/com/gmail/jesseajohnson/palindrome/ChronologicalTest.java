package com.gmail.jesseajohnson.palindrome;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.gmail.jesseajohnson.tool.Chronological;

public class ChronologicalTest {

  private Calendar calendar;
  private Chronological chronology;

  @Before
  public void init() {
    calendar = Calendar.getInstance();
  }

  @Test
  public void constructor_firstParamIsSmaller_equalsChronologyFirst() {
    Date first = calendar.getTime();
    calendar.add(Calendar.MONTH, 1);
    Date second = calendar.getTime();
    chronology = Chronological.instanceWith(first, second);
    assertEquals(first, chronology.next());
  }

  @Test
  public void constructor_secondParamIsSmaller_equalsChronologyFirst() {
    Date first = calendar.getTime();
    calendar.add(Calendar.MONTH, -1);
    calendar.add(Calendar.DAY_OF_MONTH, -1);
    Date second = calendar.getTime();
    chronology = Chronological.instanceWith(first, second);
    assertEquals(second, chronology.next());
  }

  @Test
  public void constructor_argsEqual_secondEqualsFirstAndNoSubjectsLeft() {
    Date subject = calendar.getTime();
    chronology = Chronological.instanceWith(subject, subject);
    assertEquals(subject, chronology.next());
    assertFalse(chronology.hasNext());
  }
  
  @Test
  public void hasNext_calledFirst_primaryCourse() {
    Date first = calendar.getTime();
    Date second = calendar.getTime();
    chronology = Chronological.instanceWith(first, second);
    chronology.hasNext();
  }
}