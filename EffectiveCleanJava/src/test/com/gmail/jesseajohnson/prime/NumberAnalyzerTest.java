package com.gmail.jesseajohnson.prime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class NumberAnalyzerTest {
	
	private boolean isPrime(double value) {
		return new NumberAnalyzer(value).analyze();
	}
	
	@Test
	public void analyze_3_isPrime() {		
		assertTrue(isPrime(3.0));
	}
	
	@Test
	public void analyze_4_isNotPrime() {
		assertFalse(isPrime(4.0));
	}
	
	@Test
	public void analyze_5_isNotPrime() {
		assertTrue(isPrime(5.0));
	}
	
	@Test
	public void analyze_6_isNotPrime() {
		assertFalse(isPrime(6.0));
	}
	
	@Test
	public void analyze_7_isNotPrime() {
		assertTrue(isPrime(7.0));
	}
}
