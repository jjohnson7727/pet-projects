# README #

Effective - Clean - Java

### What is this repository for? ###

A case study in all things that make software engineering effective and most importantly, clean!
Where all projects are ongoing, only Palindrome is considered effictive and clean. Other projects which were written before the study began, are in flux waiting to be given proper attention through extraction and the TDD approach.

Projects in flux:

* Prime - a prime number cruncher
* Baselining Iteration - a measurement of various looping patterns in Java

Projects planned:

* Sudoku - a process to solve a given unfinished map


### Who do I talk to? ###

* Repo owner and admin - Jesse Johnson
* Contact jesseajohnson@gmail.com or 843-300-9335