if(!window.JJ7727) {
    JJ7727 = {};
}

JJ7727.highlighter = {
    initializer: function() {
        console.log("initializer");
        var bool = this.isBrowserCompatable();
        console.log("isBrowserCompatable: " + bool);
        if(bool) {
            $(".button").mousedown(function(node) {$(node.currentTarget).addClass("active");});
            $(".button").mouseup(function(node) {$(node.currentTarget).removeClass("active");});
            $(".button").mouseout(function(node) {$(node.currentTarget).removeClass("active");});

            $('#menuLock').on("click", $.proxy(this.toggleMenuLock, this));
            $("#data").on("mouseup", $.proxy(this.processSelection, this));

            var checkclicked = function() {
                var search = $("#search").val();
                this.processText(search);
            };  $("#checktrim").on("click", $.proxy(checkclicked, this));
                $("#checkcase").on("click", $.proxy(checkclicked, this));
                $("#checkword").on("click", $.proxy(checkclicked, this));

            $("#fontsize-increase").on("click", $.proxy(this.adjustFontsize, this, 1));
            $("#fontsize-decrease").on("click", $.proxy(this.adjustFontsize, this, -1));
            $("#fontsize-reset").on("click", $.proxy(this.adjustFontsize, this, null));

            $("#search").on("keyup", $.proxy(this.preProcessText, this));
            $("#search-clear").on("click", $.proxy(this.publishData, this));

            this.listenForOpen();
            this.publishData();
        } else {
            this.publishData(false);
        }
    },

    toggleMenuLock: function() {
        $('#menuLock').toggleClass("active");
        if($('#menuLock').hasClass("active")) {
            $('html').off("mousemove");
            $("#menuContent").addClass("fixed");
        } else {
            this.listenForClose();
            $("#menuContent").removeClass("fixed");
        }
    },

    listenForOpen: function() {
        $('#menuTitle').one("mouseover", $.proxy(function(){
            $("#menuContent").slideDown(150, this.listenForClose);
        }, JJ7727.highlighter));
    },

    listenForClose: function() {
        $('html').on("mousemove", $.proxy(function(evt){
            if($(".menu").find(evt.target).length <= 0) {
                $('html').off('mousemove');
                $("#menuContent").slideUp(150, this.listenForOpen);
            }
        }, JJ7727.highlighter));
    },

    getSelected: function() {
        var selObject = null;
        var selectedText = '';

        if(window.getSelection){
            selObject = window.getSelection();
        }

        if(selObject) {
            selectedText = selObject.toString();
            if(selectedText && '' != selectedText) {
                selObject.removeAllRanges();
            }
        }

        return selectedText;
    },

    getEscapedSubject: function(subject) {
        return subject.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\\$&");
    },

    getSelectedIndices: function(data, subject) {
        if(subject && '' != subject) {
            subject = this.getEscapedSubject(subject.toString());
            var regex = new RegExp(this.getMeta(subject), this.getModifiers()), result, indices = [];
            while ( (result = regex.exec(data)) ) {
                indices.push(result.index);
            }
            return indices;
        }
    },

    highlightSubject: function(subject) {
        if(subject) {
            var indicies = this.getSelectedIndices(this.data, subject);
            if(indicies.length > 0) {
                var html = [];

                var pointer = 0;
                for(var i=0; i<indicies.length; i++) {
                    pointer = i-1 < 0 ? 0 : indicies[i-1] + subject.length;
                    html.push(this.data.substring(pointer, indicies[i]));
                    html.push("<span class='highlight'>");
                    html.push(this.data.substr(indicies[i], subject.length));
                    html.push("<\/span>");
                }

                pointer = indicies[indicies.length-1] + subject.length;
                if(pointer < this.data.length-1) {
                    html.push(this.data.substr(pointer));
                }

                $("#data").html(html.join(""));
            } else {
                $("#data").html(this.data);
            }

            $("#results").html(indicies.length);
        }
    },

    processSelection: function() {
        var subject = this.getSelected();
        if(subject && '' != subject) {
            subject = this.trim(subject);
            this.highlightSubject(subject);
            $("#search").val(subject);
        }
    },

    preProcessText: function(event) {
        if(event && event.currentTarget) {
            var subject = event.currentTarget.value;
            this.processText(subject);
        }
    },

    processText: function(subject) {
       var trimed = this.trim(subject);
       if(subject && trimed.length > 0) {
           this.highlightSubject(trimed);
       } else {
           this.publishData();
       }
    },

    adjustFontsize: function(value) {
        var fontSize;
        if(value) {
            fontSize = parseFloat($("#data").css("font-size"));
            fontSize += value;
            fontSize += "px";
        } else {
            fontSize = 'inherit';
        }

        $("#data").css({'font-size':fontSize});
    },

    publishData: function(compatible) {
        if(compatible == undefined || compatible) {
            $("#data").html(this.data);
            $("#search").val("");
            $("#results").html("");
        } else {
            $("#data").html("Please us a modern, HTML5 compatible, browser.");
        }
    },

    trim: function(subject) {
        if(subject && $("#checktrim").is(':checked'))
            return $.trim(subject);
        else return subject;
    },

    getModifiers: function() {
        return $("#checkcase").is(':checked') ? "g" : "gi";
    },

    getMeta: function(subject) {
        return $("#checkword").is(':checked') ? "\\b" + subject + "\\b" : subject;
    },

    isBrowserCompatable: function() {
        var test_canvas = document.createElement("canvas");
        return (test_canvas.getContext) ? true && this.isIECompatable() : false;
    },

    isIECompatable: function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf ("MSIE ");

        if ( msie > 0 )
           return parseInt (ua.substring (msie+5, ua.indexOf (".", msie ))) >= 9;
        else return true;
    }
};

JJ7727.highlighter.data  = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras porttitor erat libero, quis eleifend felis euismod lobortis. Donec maximus, lorem vel mollis ultricies, massa augue gravida dolor, ac malesuada turpis enim vel nibh. Nullam ac magna orci. Vivamus luctus viverra maximus. Proin sit amet lorem fermentum eros consectetur pharetra a et lorem. Praesent ullamcorper sem id justo eleifend, et luctus ligula rutrum. Cras nec elit viverra, consequat turpis pharetra, gravida lorem. Aliquam id porttitor turpis. Nullam vitae nisl ante. Praesent a eros et sem dignissim fringilla et a urna.\
\
Nulla ac enim ut ex pellentesque pellentesque. Etiam vel diam eu erat commodo imperdiet vel et felis. Donec bibendum orci magna, in dictum sapien aliquet eu. Morbi et iaculis lectus. Vivamus vehicula turpis sed dignissim eleifend. Etiam viverra consequat justo, nec consectetur est porta id. Nulla sit amet urna quam. Aliquam blandit quis metus vitae finibus. Morbi mollis nisl sit amet nisl rutrum commodo. Sed congue dui non pretium aliquet. In convallis nisi a ipsum tincidunt, in commodo massa accumsan.\
\
Suspendisse sem lacus, egestas vel vehicula ac, viverra vitae leo. Aliquam venenatis, nunc et vehicula accumsan, dolor neque blandit mi, eget hendrerit diam turpis id nunc. Integer turpis mi, ultricies eget efficitur a, maximus nec est. Vestibulum vel tempor quam. Ut at nulla dignissim, accumsan magna non, pellentesque nulla. Suspendisse commodo libero lacus, ut ullamcorper dolor scelerisque convallis. In rutrum tortor eu interdum interdum. Nullam sed leo quis dui pharetra fringilla. Praesent eleifend accumsan commodo. Proin non tellus eu arcu imperdiet interdum vel ut erat. Vivamus interdum pretium nisl, eget dapibus eros elementum eget.\
\
Mauris nec dolor ac eros pellentesque convallis tristique quis urna. Suspendisse mauris felis, condimentum ac ex at, molestie tincidunt nisi. Donec ornare nulla vitae neque elementum aliquam. In eu eros lacinia, aliquet quam eu, viverra lectus. Pellentesque sit amet hendrerit orci, eu tristique sapien. Pellentesque eget maximus lorem. Integer fermentum vestibulum dui id tincidunt. Nam iaculis porttitor dolor, in condimentum ipsum laoreet posuere. Sed finibus, eros iaculis tempus finibus, libero orci sagittis mauris, eget tristique orci augue dignissim lacus. Curabitur sodales leo sem, eget maximus lacus vestibulum nec. Sed ac feugiat augue, non sodales mi.\
\
Fusce feugiat varius diam sit amet varius. Quisque quis ultrices nisl. Mauris at magna vitae lacus elementum scelerisque. Pellentesque et quam ipsum. Aenean posuere elit nec orci blandit, id scelerisque neque pharetra. Praesent tempus augue in quam euismod, quis egestas libero sollicitudin. Nullam venenatis sagittis libero vel venenatis. Nunc maximus, erat ac tristique ultrices, nulla lacus facilisis lectus, vel imperdiet lectus sapien vel lectus. Suspendisse est lectus, tempus sit amet volutpat a, finibus pellentesque nisi. Nulla porttitor pellentesque interdum. Praesent interdum purus est, non maximus nisi fermentum non. Duis leo orci, consequat vulputate imperdiet a, laoreet in risus. Quisque aliquam erat est, id congue metus faucibus hendrerit. In quis congue magna.\
\
Ut convallis tellus lacus, sit amet lobortis nulla rutrum vitae. Integer eu nibh fermentum, rutrum nisl nec, dapibus sapien. Phasellus laoreet tellus lacus, in tincidunt eros commodo in. Duis velit neque, porta vitae tortor sed, facilisis tempus est. Nulla facilisi. Cras et justo pharetra, molestie sem a, accumsan diam. Cras fringilla ipsum turpis, at bibendum elit consequat sed. Ut et velit a leo tempus hendrerit. Curabitur vitae nunc at mi elementum condimentum. Maecenas maximus, magna a tristique convallis, turpis ante ultrices elit, ac ultricies libero purus ac turpis. Sed eget sem ac nibh volutpat posuere ut ut tellus. Donec viverra libero eget velit consequat, id maximus ante tincidunt. Etiam non euismod odio. Aliquam fermentum orci sit amet arcu ullamcorper, vel cursus mi condimentum. Praesent nec est eu nibh eleifend fringilla.\
\
Fusce ut libero interdum, sollicitudin lorem quis, iaculis dui. Nam eget nisi leo. Nunc cursus porta mattis. Ut accumsan neque tristique, sagittis odio nec, imperdiet libero. Pellentesque faucibus, diam nec dignissim blandit, orci leo consectetur enim, lobortis aliquet ipsum lacus vel quam. Phasellus ullamcorper malesuada ex in rutrum. Donec at risus hendrerit elit vestibulum pretium egestas eu velit. Aenean fringilla dui eu nunc volutpat, id pretium mi convallis. Nunc ante tortor, vulputate sed hendrerit eget, gravida a massa. Donec rhoncus, libero sed dapibus tincidunt, nisl neque lacinia diam, finibus pharetra ante arcu venenatis est. Nulla sapien ligula, pulvinar eget lectus non, semper porta ligula.\
\
Sed maximus lectus eget magna accumsan, nec vulputate ipsum euismod. Curabitur imperdiet eros at luctus lacinia. Ut ut malesuada justo. Pellentesque maximus ex mollis massa lacinia, vel fermentum neque volutpat. Proin convallis fermentum risus, ut commodo nisi. Sed laoreet tristique risus vitae egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam tincidunt magna tellus, at dictum ipsum elementum in. Vestibulum ultricies vehicula pellentesque. Donec sed justo quis tellus porta commodo in nec enim. Quisque finibus augue vestibulum pharetra vulputate. Nam ut sodales elit, fermentum posuere arcu. Cras efficitur, odio vitae hendrerit congue, erat tortor feugiat risus, cursus auctor quam ipsum in elit. Morbi ut tellus non ligula commodo consectetur. Fusce quis sapien nulla.\
\
Integer tristique porttitor turpis quis facilisis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus luctus, nunc et posuere dapibus, leo metus porttitor elit, vel ullamcorper lectus ipsum quis urna. Nunc consequat condimentum nibh sollicitudin viverra. Donec hendrerit scelerisque ligula, quis malesuada nisi rutrum at. Suspendisse potenti. Phasellus non nunc justo. Fusce ut ex sit amet odio fringilla laoreet ac nec ligula. Aliquam sit amet justo eu odio fermentum convallis. Nunc rutrum viverra diam sed pretium. Duis tellus ex, lacinia sed bibendum non, facilisis nec purus. Sed sed pulvinar massa. Vivamus laoreet ut felis sit amet sollicitudin.\
\
Aenean hendrerit sed quam vel facilisis. Nullam aliquet leo at leo lobortis tincidunt. Praesent vitae turpis ac mi fermentum placerat nec vel ante. Morbi convallis orci non odio interdum condimentum. Sed maximus, diam non ornare cursus, tellus nibh pellentesque nunc, at interdum elit nibh sit amet urna. Mauris et sem hendrerit, dignissim odio sit amet, tempus mauris. Nulla ut ligula a leo ultrices posuere. Donec fermentum purus eget libero volutpat hendrerit ac nec ex. Cum sociis natoque.";
