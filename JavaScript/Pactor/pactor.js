if(!window.JJ7727) { JJ7727 = {}; }

JJ7727.pactor = {
    initialize: function() {
        if(typeof(Worker) !== 'undefined') {
            this.worker = new Worker("pactorWorker.js");
        }

        this.status = this.states.STOPPED;
        console.log(this.status);
    },

    processChunk: function(/*string*/chunk) {
        this.data.unshift(chunk);
    },

    states: {STOPPED: "State: Stopped", RUNNING: "State: Running"},
    status: null,
    data: new Array()
};
