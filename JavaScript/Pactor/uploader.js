if(!window.JJ7727) {JJ7727 = {};}

JJ7727.uploader = {
        filename: "uploaderWorker.js",
        initialize: function(/*Function*/callback) {
            var callbacktyp = typeof callback;
            if(callbacktyp !== 'function') {
                throw "[Invalid Parameter Exception] in: JJ7727.uploader.initialize(callback) only accepts a function type argument. Found: " + callbacktyp;
            } else {
                this.callback = callback;
            }

            if(typeof(Worker) !== 'undefined') {
                this.worker = new Worker(this.filename);

                var process = $.proxy(this.processMessage, this);
                this.worker.onmessage = process;

                $('#dropzone').on('dragover dragenter', false);

                var post = $.proxy(this.postMessage, this);
                $('#dropzone')[0].addEventListener('drop', post, false);
            } else {
                $('body').html("No browser support for this app.");
            }
        },

        terminate: function() {
            this.worker.terminate();
        },

        processMessage: function(messageEvent) {
            this.worker.terminate();
            $('#data').html("");
            var element = $('#data')[0];
            element.innerHTML = messageEvent.data.text;
            $('#fileinfo').text(messageEvent.data.name);
            this.callback(messageEvent.data.name);
        },

        postMessage: function(event) {
            event.preventDefault();
            event.stopPropagation();
            var files = event.dataTransfer.files || event.target.files;
            this.worker.postMessage({'files' : files});
        },

        worker: null,
        callback: null
};
