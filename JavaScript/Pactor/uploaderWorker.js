this.onmessage = function(event) {
    upload(event.data.files);
};

this.onerror = function(event) {
    console.log(event);
};

function upload(files) {
    var response = {
        event:null,
        name:null,
        text:null
    };

    for (var x = 0, file; file = files[x]; x++) {
        var fileReader = new FileReader();
        fileReader.onload = (function(file) {
            response.name = file.name;
            return function(e) {
                response.text = e.target.result;
                postMessage(response);
            };
        })(file);
        fileReader.readAsText(file);
    }
}
